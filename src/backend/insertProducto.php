<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    include_once 'validarData.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    // Datos Producto
    $nombreProducto=$data['nombre'];
    $categoriaProducto=$data['categoria'];
    $imgProducto=$data['imagen'];
    $horasTrabajo=$data['horasTrabajo'];
    $precioMinorista=$data['precioMinorista'];
    $precioMayorista=$data['precioMayorista'];
    $year=$data['year'];
    $insumos=$data['insumos'];
    $packaging=$data['packaging'];
    $comb=$data['combinaciones'];

    
    $sql="INSERT INTO productos(nombre,imagen,idcategorias,horasTrabajo,precioMinorista,precioMayorista,`year`) VALUES (?,?,?,?,?,?,?)";
    $sql_insert=$pdo->prepare($sql);
    $sql_insert->execute(array($nombreProducto,$imgProducto,$categoriaProducto,$horasTrabajo,$precioMinorista,$precioMayorista,$year));
    if($sql_insert){
        $sql="SELECT MAX(idproductos) AS id FROM productos";
        $idproducto=$pdo->prepare($sql);
        $idproducto->execute();
        $idproducto=$idproducto->fetch();
        $idproducto=$idproducto['id'];
        // Cargar los elementos de packaging del priducto
        for($i=0;$i<sizeof($packaging);$i++){
            $item=$packaging[$i];
            $idpack=$item['idpackaging'];
            $sql="INSERT INTO packaging_productos(packaging_idpackaging,productos_idproductos) VALUES (?,?)";
            $insert_pack_prod=$pdo->prepare($sql);
            $insert_pack_prod->execute(array($idpack,$idproducto));
        }
        // Cargar los insumos del priducto
        for($i=0;$i<sizeof($insumos);$i++){
            $item=$insumos[$i];
            $idinsumo=$item['idinsumos'];
            $cantInsumo=$item['cantidadInsumo'];
            $sql="INSERT INTO insumos_por_productos(productos_idproductos,insumos_idinsumos,cantidadInsumo) VALUES (?,?,?)";
            $insert_ins_prod=$pdo->prepare($sql);
            $insert_ins_prod->execute(array($idproducto,$idinsumo,$cantInsumo));
        }
        // Cargar las combinaciones del priducto
        for ($i=0; $i < sizeof($comb); $i++) { 
            $item=$comb[$i];
            $prim=$item['primario'];
            $sec=$item['secundario'];
            $sql="INSERT INTO combinaciones_prod(productos_idproductos) VALUES (?)";
            $insert_comb_prod=$pdo->prepare($sql);
            $insert_comb_prod->execute(array($idproducto));
            if($insert_comb_prod){
                //obtengo el id de la combinacion
                $sql="SELECT MAX(idcombinaciones_prod) AS id FROM combinaciones_prod";
                $idcombinaciones_prod=$pdo->prepare($sql);
                $idcombinaciones_prod->execute();
                $idcombinaciones_prod=$idcombinaciones_prod->fetch();
                $idcombinaciones_prod=$idcombinaciones_prod['id'];
                //Inserto la combinacion primaria
                    $tipo=1;
                    $sql="INSERT INTO combinacion_tipo(idcombinaciones_prod,tipo_comb) VALUES (?,?)";
                    $insert_comb_tipo=$pdo->prepare($sql);
                    $insert_comb_tipo->execute(array($idcombinaciones_prod,$tipo));
                    if($insert_comb_tipo){
                        // obtengo el id del tipo de combinacion
                        $sql="SELECT MAX(idcombinacion_tipo) AS id FROM combinacion_tipo";
                        $idcombinacion_tipo=$pdo->prepare($sql);
                        $idcombinacion_tipo->execute();
                        $idcombinacion_tipo=$idcombinacion_tipo->fetch();
                        $idcombinacion_tipo=$idcombinacion_tipo['id'];
                        // inserto el color de la combinacion
                        $color=$prim['color'];
                        $sql="INSERT INTO combinacion_color(idcombinacion_tipo,color) VALUES (?,?)";
                        $insert_comb_color=$pdo->prepare($sql);
                        $insert_comb_color->execute(array($idcombinacion_tipo,$color));
                        if($insert_comb_color){
                            // obtengo el id del color de combinacion
                            $sql="SELECT MAX(idcombinacion_color) AS id FROM combinacion_color";
                            $idcombinacion_color=$pdo->prepare($sql);
                            $idcombinacion_color->execute();
                            $idcombinacion_color=$idcombinacion_color->fetch();
                            $idcombinacion_color=$idcombinacion_color['id'];
                            // inserto la lista de insumos
                            $insumos_comb=$prim['insumos_idinsumos'];
                            for($j=0; $j < sizeof($insumos_comb); $j++) { 
                                $insumos_comb_id=$insumos_comb[$j];
                                $sql="INSERT INTO lista_insumos_comb(idcombinacion_color,insumos_idinsumos) VALUES (?,?)";
                                $insert_comb_color=$pdo->prepare($sql);
                                $insert_comb_color->execute(array($idcombinacion_color,$insumos_comb_id));
                            }
                        }
                    }
                //Fin combinacion primaria
                
                //Inserto la combinacion secundaria
                $tipo=2;
                $sql="INSERT INTO combinacion_tipo(idcombinaciones_prod,tipo_comb) VALUES (?,?)";
                $insert_comb_tipo=$pdo->prepare($sql);
                $insert_comb_tipo->execute(array($idcombinaciones_prod,$tipo));
                if($insert_comb_tipo){
                    // obtengo el id del tipo de combinacion
                    $sql="SELECT MAX(idcombinacion_tipo) AS id FROM combinacion_tipo";
                    $idcombinacion_tipo=$pdo->prepare($sql);
                    $idcombinacion_tipo->execute();
                    $idcombinacion_tipo=$idcombinacion_tipo->fetch();
                    $idcombinacion_tipo=$idcombinacion_tipo['id'];
                    // inserto el color de la combinacion
                    $color=$sec['color'];
                    $sql="INSERT INTO combinacion_color(idcombinacion_tipo,color) VALUES (?,?)";
                    $insert_comb_color=$pdo->prepare($sql);
                    $insert_comb_color->execute(array($idcombinacion_tipo,$color));
                    if($insert_comb_color){
                        // obtengo el id del color de combinacion
                        $sql="SELECT MAX(idcombinacion_color) AS id FROM combinacion_color";
                        $idcombinacion_color=$pdo->prepare($sql);
                        $idcombinacion_color->execute();
                        $idcombinacion_color=$idcombinacion_color->fetch();
                        $idcombinacion_color=$idcombinacion_color['id'];
                        // inserto la lista de insumos
                        $insumos_comb=$sec['insumos_idinsumos'];
                        for($j=0; $j < sizeof($insumos_comb); $j++) { 
                            $insumos_comb_id=$insumos_comb[$j];
                            $sql="INSERT INTO lista_insumos_comb(idcombinacion_color,insumos_idinsumos) VALUES (?,?)";
                            $insert_comb_color=$pdo->prepare($sql);
                            $insert_comb_color->execute(array($idcombinacion_color,$insumos_comb_id));
                        }
                    }
                }
            //Fin combinacion secundaria
            }
        }

        $response = ["Data" => '<strong>Correcto!</strong> '.$nombreProducto.' se ha registrado correctamente.' , "Estado" => true];
    }else{
        $response = ["Data" => '<strong>Error!</strong> No se ha podido registrar el producto: '.$nombreProducto , "Estado" => false];
    }
    

    echo json_encode($response);
?>