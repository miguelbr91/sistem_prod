<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    $idColor=$data["idcolores"];
    $idInsumo=$data["insumos_idinsumos"];

    $sql="SELECT COUNT(*) AS cant FROM colores WHERE insumos_idinsumos = (?)";
    $cantidad_colores=$pdo->prepare($sql);
    $cantidad_colores->execute(array($idInsumo));
    $cantidad_colores=$cantidad_colores->fetch();
    $cant=$cantidad_colores['cant'];

    
    if($cant>1){
        $sql="DELETE FROM colores WHERE idcolores = (?)";
        $delete_sql=$pdo->prepare($sql);
        $delete_sql->execute(array($idColor));
    }else{
        $sql="DELETE FROM colores WHERE idcolores = (?)";
        $delete_sql=$pdo->prepare($sql);
        $delete_sql->execute(array($idColor));
        $sql="DELETE FROM insumos WHERE idinsumos = (?)";
        $delete_sql=$pdo->prepare($sql);
        $delete_sql->execute(array($idInsumo));
    }

    if($delete_sql){
        $response = ["Data" => 'El insumo se ha eliminado correctamente.' , "Estado" => true];
    }else{
        $response = ["Data" => 'Error! no se pudo eliminar el insumo' , "Estado" => false];
    }

    $response = json_encode($response);
    echo $response; 
?>