<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    session_start();
    $usr = $_SESSION['usuario'];
    if(!empty($usr)){
        $usr = [
            "name" => $_SESSION['usuario'],
            "mail" => $_SESSION['mail'],
            "type" => $_SESSION['type']
        ];
        echo json_encode($usr);
    }else{
        $usr = ["name" => $_SESSION['usuario']];
        echo json_encode($usr);
    }
?>