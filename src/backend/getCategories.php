<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';

    $sql="SELECT DISTINCT categoria FROM insumos ORDER BY categoria ASC";
    $get_categories=$pdo->prepare($sql);
    $get_categories->execute();
    $get_categories=$get_categories->fetchAll();
    $index=0;
    $bg=0;
    foreach ($get_categories as $item) {
        switch($bg){
            case 0: $color='bg-green';break;
            case 1: $color='bg-cyan';break;
            case 2: $color='bg-orange';break;
            case 3: $color='bg-red';break;
            case 4: $color='bg-ambar';break;
            case 5: $color='bg-pink';break;
        }
        $response[$index]=["categoria" => $item['categoria'], "bg" => $color];
        $index++;
        $bg++;
        if($bg>6){
            $bg=0;
        }
    }
    echo json_encode($response);
?>