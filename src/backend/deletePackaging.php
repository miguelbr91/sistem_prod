<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    $id=$data["idpackaging"];
    
    $sql="DELETE FROM packaging WHERE idpackaging = (?)";
    $delete_sql=$pdo->prepare($sql);
    $delete_sql->execute(array($id));
    
    if($delete_sql){
        $response = ["message" => 'Se ha eliminado correctamente.' , "estado" => true];
    }else{
        $response = ["message" => 'Error! no se pudo eliminar' , "estado" => false];
    }

    $response = json_encode($response);
    echo $response; 
?>