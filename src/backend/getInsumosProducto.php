<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $parametro=$data['parametro'];

    // Listar insumos
    if(empty($parametro) || $parametro==='' || $parametro===null){
        $sql="SELECT i.* FROM insumos AS i ORDER BY i.categoria,i.nombre";
        $sql_get=$pdo->prepare($sql);
        $sql_get->execute();
    }else{
        $sql="SELECT i.* FROM insumos AS i WHERE ((i.categoria LIKE '%$parametro%') OR (i.nombre LIKE '%$parametro%') OR (i.detalle LIKE '%$parametro%')) ORDER BY i.categoria,i.nombre";
        $sql_get=$pdo->prepare($sql);
        $sql_get->execute();
    }
    
    $response=$sql_get->fetchAll();
    echo json_encode($response);
?>