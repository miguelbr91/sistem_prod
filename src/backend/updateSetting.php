<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    $gananciaAros=$data['porcentajeGananciaAros'];
    $gananciaCollares=$data['porcentajeGananciaCollares'];
    $gananciaPulseras=$data['porcentajeGananciaPulseras'];
    $precioHoraAros=$data['precioHoraAros'];
    $precioHoraCollares=$data['precioHoraCollares'];
    $precioHoraPulseras=$data['precioHoraPulseras'];

    for ($i=1; $i <4 ; $i++) { 
        $sql="UPDATE setting SET porcentajeGanancia=?,precioHoras=?  WHERE idsetting = ?";
        $update_setting=$pdo->prepare($sql);
        switch($i){
            case 1:$update_setting->execute(array($gananciaAros,$precioHoraAros,$i));break;
            case 2:$update_setting->execute(array($gananciaCollares,$precioHoraCollares,$i));break;
            case 3:$update_setting->execute(array($gananciaPulseras,$precioHoraPulseras,$i));break;
        }
        
    }
    
    if($update_setting){
        $response = ["Data" => '<strong>Correcto!</strong> Se ha actualizado correctamente.' , "Estado" => true];
    }else{
        $response = ["Data" => '<strong>Error!</strong> no se pudo actualizar' , "Estado" => false];
    }

    $response = json_encode($response);
    echo $response; 
?>