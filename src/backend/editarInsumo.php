<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    require_once 'validarData.php';

    $idInsumo=$data['idinsumos'];
    $idColor=$data['idcolores'];
    $codInsumo=$data['codigoInsumo'];
    $categoria=strtoupper(validar($data['categoria']));
    $proveedor=validar($data['proveedores_idproveedor']);
    $nombre=strtoupper(validar($data['nombre']));
    $detalle=strtoupper(validar($data['detalle']));
    $unidadMedida=validar($data['unidadMedida']);
    $presentacion=strtoupper(validar($data['presentacion']));
    $unidadesPresentacion=validar($data['unidadesPresentacion']);
    $costoPresentacion=validar($data['costoPresentacion']);
    $stock=validar($data['stock']);
    $color=strtoupper(validar($data['color']));
    $costoUnitario=validar($data['costoUnitario']);

    if($codInsumo===null){
        $codInsumo=$idInsumo;
    }

    $sql="UPDATE insumos SET proveedores_idproveedor = ?,codigoInsumo = ?,categoria = ?,nombre = ?,detalle = ?,unidadMedida = ?,presentacion = ?,costoPresentacion = ?,unidadesPresentacion = ?,costoUnitario = ?  WHERE idinsumos = ?";
    $update_insumo=$pdo->prepare($sql);
    $update_insumo->execute(array($proveedor,$codInsumo,$categoria,$nombre,$detalle,$unidadMedida,$presentacion,$costoPresentacion,$unidadesPresentacion,$costoUnitario,$idInsumo));

    $sql="UPDATE colores SET color=?,stock=?  WHERE idcolores = ?";
    $update_color=$pdo->prepare($sql);
    $update_color->execute(array($color,$stock,$idColor));
    
    if($update_insumo && $update_color){
        $response = ["Data" => 'Se ha actualizado correctamente.' , "Estado" => true];
    }else{
        $response = ["Data" => 'Error! no se pudo actualizar' , "Estado" => false];
    }

    $response = json_encode($response);
    echo $response; 
?>