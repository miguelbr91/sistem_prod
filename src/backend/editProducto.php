<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    include_once 'validarData.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    // Datos Producto
    $idproductos=(int)$data['idproductos'];
    $nombreProducto=$data['nombre'];
    $categoriaProducto=$data['idcategorias'];
    $imgProducto=$data['imagen'];
    $precioMinorista=$data['precioMinorista'];
    $precioMayorista=$data['precioMayorista'];
    $horas=$data['horasTrabajo'];
    $insumos=$data['insumos'];
    $pack=$data['packaging'];
    $comb=$data['combinaciones'];
    $year=$data['year'];

    $sql="UPDATE productos SET nombre=?,imagen=?,idcategorias=?,precioMinorista=?,precioMayorista=?,horasTrabajo=?,`year`=? WHERE idproductos=?";
    $sql_update=$pdo->prepare($sql);
    $sql_update->execute(array($nombreProducto,$imgProducto,$categoriaProducto,$precioMinorista,$precioMayorista,$horas,$year,$idproductos));
    if($sql_update){
        //Borrar todos los insumos del producto
        $sql2="DELETE FROM insumos_por_productos WHERE productos_idproductos=$idproductos";
        $sql_delete_insumos=$pdo->prepare($sql2);
        $sql_delete_insumos->execute();
        //---------------------------------------
        //Cargar los nuevos insumos del producto
        if($sql_delete_insumos){
            for ($j=0; $j < sizeof($insumos); $j++) {
                $item=$insumos[$j];
                $idinsumo=(int)$item['insumos_idinsumos'];
                $cantInsumo=(float)$item['cantidadInsumo'];
                $sql="INSERT INTO insumos_por_productos VALUES (?,?,?)";
                $insert_ins_prod=$pdo->prepare($sql);
                $insert_ins_prod->execute(array($idproductos,$idinsumo,$cantInsumo));
            }
        }
        //---------------------------------------
        //Borrar todos los packaging del producto
        $sql3="DELETE FROM packaging_productos WHERE productos_idproductos=$idproductos";
        $sql_delete_pack=$pdo->prepare($sql3);
        $sql_delete_pack->execute();
        //---------------------------------------
        //Cargar los nuevos packaging del producto
        if($sql_delete_pack){
            for ($i=0; $i < sizeof($pack); $i++) { 
                $item=$pack[$i];
                $idpack=(int)$item['idpackaging'];
                $sql="INSERT INTO packaging_productos(packaging_idpackaging,productos_idproductos) VALUES (?,?)";
                $insert_pack_prod=$pdo->prepare($sql);
                $insert_pack_prod->execute(array($idpack,$idproductos));
            }
        }
        //---------------------------------------
        //Borrar todos las combinaciones del producto
        $sql4="DELETE FROM combinaciones_prod WHERE productos_idproductos=$idproductos";
        $sql_delete_comb=$pdo->prepare($sql4);
        $sql_delete_comb->execute();
        //---------------------------------------
        //Cargar las nuevos combinaciones del producto
        if($sql_delete_comb){
            for ($i=0; $i < sizeof($comb); $i++) { 
                $item=$comb[$i];
                $prim=$item['primario'];
                $sec=$item['secundario'];
                $sql="INSERT INTO combinaciones_prod(productos_idproductos) VALUES (?)";
                $insert_comb_prod=$pdo->prepare($sql);
                $insert_comb_prod->execute(array($idproductos));
                if($insert_comb_prod){
                    //obtengo el id de la combinacion
                    $sql="SELECT MAX(idcombinaciones_prod) AS id FROM combinaciones_prod";
                    $idcombinaciones_prod=$pdo->prepare($sql);
                    $idcombinaciones_prod->execute();
                    $idcombinaciones_prod=$idcombinaciones_prod->fetch();
                    $idcombinaciones_prod=$idcombinaciones_prod['id'];
                    //Inserto la combinacion primaria
                        $tipo=1;
                        $sql="INSERT INTO combinacion_tipo(idcombinaciones_prod,tipo_comb) VALUES (?,?)";
                        $insert_comb_tipo=$pdo->prepare($sql);
                        $insert_comb_tipo->execute(array($idcombinaciones_prod,$tipo));
                        if($insert_comb_tipo){
                            // obtengo el id del tipo de combinacion
                            $sql="SELECT MAX(idcombinacion_tipo) AS id FROM combinacion_tipo";
                            $idcombinacion_tipo=$pdo->prepare($sql);
                            $idcombinacion_tipo->execute();
                            $idcombinacion_tipo=$idcombinacion_tipo->fetch();
                            $idcombinacion_tipo=$idcombinacion_tipo['id'];
                            // inserto el color de la combinacion
                            $color=$prim['color'];
                            $sql="INSERT INTO combinacion_color(idcombinacion_tipo,color) VALUES (?,?)";
                            $insert_comb_color=$pdo->prepare($sql);
                            $insert_comb_color->execute(array($idcombinacion_tipo,$color));
                            if($insert_comb_color){
                                // obtengo el id del color de combinacion
                                $sql="SELECT MAX(idcombinacion_color) AS id FROM combinacion_color";
                                $idcombinacion_color=$pdo->prepare($sql);
                                $idcombinacion_color->execute();
                                $idcombinacion_color=$idcombinacion_color->fetch();
                                $idcombinacion_color=$idcombinacion_color['id'];
                                // inserto la lista de insumos
                                $insumos_comb=$prim['insumos_idinsumos'];
                                for($j=0; $j < sizeof($insumos_comb); $j++) { 
                                    $insumos_comb_id=$insumos_comb[$j];
                                    $sql="INSERT INTO lista_insumos_comb(idcombinacion_color,insumos_idinsumos) VALUES (?,?)";
                                    $insert_comb_color=$pdo->prepare($sql);
                                    $insert_comb_color->execute(array($idcombinacion_color,$insumos_comb_id));
                                }
                            }
                        }
                    //Fin combinacion primaria
                    
                    //Inserto la combinacion secundaria
                    $tipo=2;
                    $sql="INSERT INTO combinacion_tipo(idcombinaciones_prod,tipo_comb) VALUES (?,?)";
                    $insert_comb_tipo=$pdo->prepare($sql);
                    $insert_comb_tipo->execute(array($idcombinaciones_prod,$tipo));
                    if($insert_comb_tipo){
                        // obtengo el id del tipo de combinacion
                        $sql="SELECT MAX(idcombinacion_tipo) AS id FROM combinacion_tipo";
                        $idcombinacion_tipo=$pdo->prepare($sql);
                        $idcombinacion_tipo->execute();
                        $idcombinacion_tipo=$idcombinacion_tipo->fetch();
                        $idcombinacion_tipo=$idcombinacion_tipo['id'];
                        // inserto el color de la combinacion
                        $color=$sec['color'];
                        $sql="INSERT INTO combinacion_color(idcombinacion_tipo,color) VALUES (?,?)";
                        $insert_comb_color=$pdo->prepare($sql);
                        $insert_comb_color->execute(array($idcombinacion_tipo,$color));
                        if($insert_comb_color){
                            // obtengo el id del color de combinacion
                            $sql="SELECT MAX(idcombinacion_color) AS id FROM combinacion_color";
                            $idcombinacion_color=$pdo->prepare($sql);
                            $idcombinacion_color->execute();
                            $idcombinacion_color=$idcombinacion_color->fetch();
                            $idcombinacion_color=$idcombinacion_color['id'];
                            // inserto la lista de insumos
                            $insumos_comb=$sec['insumos_idinsumos'];
                            for($j=0; $j < sizeof($insumos_comb); $j++) { 
                                $insumos_comb_id=$insumos_comb[$j];
                                $sql="INSERT INTO lista_insumos_comb(idcombinacion_color,insumos_idinsumos) VALUES (?,?)";
                                $insert_comb_color=$pdo->prepare($sql);
                                $insert_comb_color->execute(array($idcombinacion_color,$insumos_comb_id));
                            }
                        }
                    }
                //Fin combinacion secundaria
                }
            }
        }
        //---------------------------------------
        $response = ["Data" => '<strong>Correcto!</strong> '.$nombreProducto.' se actualizó correctamente.' , "Estado" => true];
    }else{
        $response = ["Data" => '<strong>Error!</strong> No se ha podido actualizar el producto: '.$nombreProducto , "Estado" => false];
    }

    echo json_encode($response);
?>