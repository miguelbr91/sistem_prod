<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';

    $sql="SELECT DISTINCT color FROM colores ORDER BY color ASC";
    $get_categories=$pdo->prepare($sql);
    $get_categories->execute();
    $response=$get_categories->fetchAll();
    
    echo json_encode($response);
?>