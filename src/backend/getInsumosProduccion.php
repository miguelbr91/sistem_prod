<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $parametro=$data['parametro'];
    
    // Listar Colores insumo
    if(empty($parametro) || $parametro==='' || $parametro===null){
        $sql="SELECT * FROM vw_insumosProduction";
    }else{
        $sql="SELECT * FROM vw_insumosProduction WHERE production_idproduction=$parametro";
    }
    $sql_get=$pdo->prepare($sql);
    $sql_get->execute();
    $response=$sql_get->fetchAll();
    echo json_encode($response);
?>