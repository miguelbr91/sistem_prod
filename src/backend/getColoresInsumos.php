<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $parametro=$data['parametro'];
    $response=[];

    // Listar Colores insumo
    if(empty($parametro) || $parametro==='' || $parametro===null){
        $response=null;
    }else{
        $sql="SELECT ixp.insumos_idinsumos,ixp.nombre,ixp.categoria,ixp.detalle FROM productos as pr inner join insumosXproductos as ixp ON pr.idproductos=ixp.productos_idproductos WHERE pr.idproductos=$parametro";
        //$sql="SELECT ixp.insumos_idinsumos,ixp.nombre,ixp.categoria,ixp.detalle,c.idcolores,c.color FROM productos as pr inner join insumosXproductos as ixp inner join colores as c ON pr.idproductos=ixp.productos_idproductos AND ixp.insumos_idinsumos=c.insumos_idinsumos WHERE pr.idproductos=$parametro";
        $sql_get=$pdo->prepare($sql);
        $sql_get->execute();
        
        while($insumos=$sql_get->fetch(PDO::FETCH_ASSOC)){
            $idinsumo=$insumos['insumos_idinsumos'];
            $sql="SELECT c.idcolores,c.color FROM colores as c WHERE c.insumos_idinsumos=$idinsumo";
            $sql_get_colores=$pdo->prepare($sql);
            $sql_get_colores->execute();
            $colores=$sql_get_colores->fetchAll();
            $item=[
                "idinsumos"=>$insumos['insumos_idinsumos'],
                "nombre"=>$insumos['nombre'],
                "categoria"=>$insumos['categoria'],
                "detalle"=>$insumos['detalle'],
                "color"=>null,
                "colores"=>$colores
            ];
            array_push($response,$item);
        }
    }
    


    echo json_encode($response);
?>