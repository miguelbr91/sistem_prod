<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    require_once 'validarData.php';

    $id=$data['idproveedor'];
    $nombre=validar($data['nombre']);
    $nombreContacto=validar($data['nombreContacto']);
    $mail=validar($data['mail']);
    $web=validar($data['web']);
    $direccion=validar($data['direccion']);
    $ciudad=validar($data['ciudad']);
    $provincia=validar($data['provincia']);
    $comentario=validar($data['comentario']);
    $telefono=validar($data['telefono']);
    $celular=validar($data['celular']);

    $sql="UPDATE proveedores SET nombre=?,nombreContacto=?,mail=?,web=?,direccion=?,ciudad=?,provincia=?,comentario=?,telefono=?,celular=?  WHERE idproveedor = ?";
    $update_sql=$pdo->prepare($sql);
    $update_sql->execute(array($nombre,$nombreContacto,$mail,$web,$direccion,$ciudad,$provincia,$comentario,$telefono,$celular,$id));
    
    if($update_sql){
        $response = ["Data" => 'Se ha actualizado correctamente.' , "Estado" => true];
    }else{
        $response = ["Data" => 'Error! no se pudo actualizar' , "Estado" => false];
    }

    $response = json_encode($response);
    echo $response; 
?>