<?php

    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';

    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $parametro=$data['parametro'];

    if(empty($parametro) || $parametro==='' || $parametro===null){
        $sql="SELECT * FROM vw_productos ORDER BY categoria,nombre";
        $gsent=$pdo->prepare($sql);
        $gsent->execute();
    }else{
        $sql="SELECT * FROM vw_productos WHERE nombre LIKE '%$parametro%' OR categoria LIKE '%$parametro%' ORDER BY categoria,nombre";
        $gsent=$pdo->prepare($sql);
        $gsent->execute();
    }
    
    $response=$gsent->fetchAll();
    
    echo json_encode($response);

?>