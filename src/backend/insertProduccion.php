<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';

    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    $categoria=$data[0]['idcategorias'];
    $totalProduccion=$data[0]['cantidadProducir'];
    $totalProducida=0;
    $estado=0;
    $mesNro=$data[0]['mesNro'];
    $mesNombre=$data[0]['mesNombre'];
    $year=$data[0]['year'];
    $productos=$data;

    $sql="SELECT * FROM production WHERE `mesNro`=? AND `year`=?";
    $sql_id=$pdo->prepare($sql);
    $sql_id->execute(array($mesNro,$year));
    $idpd=$sql_id->fetch();
    $idpd=$idpd["idproduction"];
    
    if($idpd!=null){
        // verificar existencia de categoria
        $sql="SELECT * FROM categoria_por_production WHERE `production_idproduction`=? AND `categorias_idcategorias`=?";
        $sql_cxp=$pdo->prepare($sql);
        $sql_cxp->execute(array($idpd,$categoria));
        $cxp=$sql_cxp->fetchAll();
        if(!empty($cxp)){
            // existe la categoria devolver mensaje de error no se puede cargar nuevo
            $response = ["Data" => '<strong>Error!</strong> No se ha podido registrar. Ya se encuentra cargada la producción de esa categoria' , "Estado" => false];
        }else{
            // no existe la categoria
            // insertar categoria por produccion
            $sql="INSERT INTO categoria_por_production(production_idproduction,categorias_idcategorias,cantidadProducir,cantidadProducida) VALUES (?,?,?,?)";
            $sql_insert_cat=$pdo->prepare($sql);
            $sql_insert_cat->execute(array($idpd,$categoria,$totalProduccion,$totalProducida));

            // obtener el ultimo id de la tabla categoria_por_production
            $sql="SELECT MAX(idCategoriaProduccion) AS id FROM categoria_por_production";
            $idcxp=$pdo->prepare($sql);
            $idcxp->execute();
            $idcxp=$idcxp->fetch();
            $idcxp=$idcxp['id'];

            // cargar los productos por color y cantidad
            for ($i=0; $i < sizeof($productos); $i++) { 
                $item=$productos[$i];
                if(($item['totalProducto'])>0){
                    $idproducto=$item['idproductos'];
                    $colores=$item['colores'];
                    for ($j=0; $j < sizeof($colores); $j++) { 
                        $elto=$colores[$j];
                        $orden=$elto['orden'];
                        $nomColor=$elto['color'];
                        $cant=$elto['cantidad'];
                        $sql="INSERT INTO productos_por_produccion(productos_idproductos,idCategoriaProduccion,color,cantidad,realizado,orden) VALUES (?,?,?,?,?,?)";
                        $insert_insum_prod=$pdo->prepare($sql);
                        $insert_insum_prod->execute(array($idproducto,$idcxp,$nomColor,$cant,$totalProducida,$orden));
                    }
                }
            }
            $response = ["Data" => '<strong>Correcto!</strong> se ha registrado correctamente.' , "Estado" => true];
        }
    }else{
        // crear produccion y añadir categorias
        $sql="INSERT INTO production(estado,`year`,mesNro,mesNombre) values(?,?,?,?)";
        $sql_insert=$pdo->prepare($sql);
        $sql_insert->execute(array($estado,$year,$mesNro,$mesNombre));

        // insertar productos por produccion
        if($sql_insert){
            // obtener ultimo id produccion
            $sql="SELECT MAX(idproduction) AS id FROM production";
            $idproduction=$pdo->prepare($sql);
            $idproduction->execute();
            $idproduction=$idproduction->fetch();
            $idproduction=$idproduction['id'];

            // insertar categoria por produccion
            $sql="INSERT INTO categoria_por_production(production_idproduction,categorias_idcategorias,cantidadProducir) VALUES (?,?,?)";
            $sql_insert_cat=$pdo->prepare($sql);
            $sql_insert_cat->execute(array($idproduction,$categoria,$totalProduccion));

            // obtener el ultimo id de la tabla categoria_por_production
            $sql="SELECT MAX(idCategoriaProduccion) AS id FROM categoria_por_production";
            $idcxp=$pdo->prepare($sql);
            $idcxp->execute();
            $idcxp=$idcxp->fetch();
            $idcxp=$idcxp['id'];

            // cargar los productos por color y cantidad
            for ($i=0; $i < sizeof($productos); $i++) { 
                $item=$productos[$i];
                if(($item['totalProducto'])>0){
                    $idproducto=$item['idproductos'];
                    $colores=$item['colores'];
                    for ($j=0; $j < sizeof($colores); $j++) { 
                        $elto=$colores[$j];
                        $nomColor=$elto['color'];
                        $cant=$elto['cantidad'];
                        $sql="INSERT INTO productos_por_produccion(productos_idproductos,idCategoriaProduccion,color,cantidad) VALUES (?,?,?,?)";
                        $insert_insum_prod=$pdo->prepare($sql);
                        $insert_insum_prod->execute(array($idproducto,$idcxp,$nomColor,$cant));
                    }
                }
            }
            $response = ["Data" => '<strong>Correcto!</strong> se ha registrado correctamente.' , "Estado" => true];
        }else{
            $response = ["Data" => '<strong>Error!</strong> No se ha podido registrar.' , "Estado" => false];
        }
    }


    echo json_encode($response);
?>