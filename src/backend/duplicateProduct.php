<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    // Datos Producto
    $nombreProducto=$data['nombre'];
    $categoriaProducto=$data['idcategorias'];
    $imgProducto=$data['imagen'];
    $horasTrabajo=$data['horasTrabajo'];
    $precioMinorista=$data['precioMinorista'];
    $precioMayorista=$data['precioMayorista'];
    $insumos=$data['insumos'];
    $packaging=$data['packaging'];

    
    $sql="INSERT INTO productos(nombre,imagen,idcategorias,horasTrabajo,precioMinorista,precioMayorista) VALUES (?,?,?,?,?,?)";
    $sql_insert=$pdo->prepare($sql);
    $sql_insert->execute(array($nombreProducto,$imgProducto,$categoriaProducto,$horasTrabajo,$precioMinorista,$precioMayorista));
    if($sql_insert){
        $sql="SELECT MAX(idproductos) AS id FROM productos";
        $idproducto=$pdo->prepare($sql);
        $idproducto->execute();
        $idproducto=$idproducto->fetch();
        $idproducto=$idproducto['id'];
        for($i=0;$i<sizeof($packaging);$i++){
            $item=$packaging[$i];
            $idpack=$item['idpackaging'];
            $sql="INSERT INTO packaging_productos(packaging_idpackaging,productos_idproductos) VALUES (?,?)";
            $insert_pack_prod=$pdo->prepare($sql);
            $insert_pack_prod->execute(array($idpack,$idproducto));
        }
        for($i=0;$i<sizeof($insumos);$i++){
            $item=$insumos[$i];
            $idinsumo=$item['insumos_idinsumos'];
            $cantInsumo=$item['cantidadInsumo'];
            $sql="INSERT INTO insumos_por_productos(productos_idproductos,insumos_idinsumos,cantidadInsumo) VALUES (?,?,?)";
            $insert_ins_prod=$pdo->prepare($sql);
            $insert_ins_prod->execute(array($idproducto,$idinsumo,$cantInsumo));
        }
        $response = ["Data" => '<strong>Correcto!</strong> '.$nombreProducto.' se ha duplicado correctamente.' , "Estado" => true];
    }else{
        $response = ["Data" => '<strong>Error!</strong> No se ha podido registrar duplicado del producto: '.$nombreProducto , "Estado" => false];
    }
    

    echo json_encode($response);
?>