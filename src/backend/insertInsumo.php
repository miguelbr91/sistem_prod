<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    include_once 'validarData.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    // Insertar Insumo
    $codInsumo=$data['codigoInsumo'];
    $categoria=strtoupper(validar($data['categoria']));
    $proveedor=validar($data['proveedores_idproveedor']);
    $nombre=strtoupper(validar($data['nombre']));
    $detalle=strtoupper(validar($data['detalle']));
    $unidadMedida=strtoupper(validar($data['unidadMedida']));
    $presentacion=strtoupper(validar($data['presentacion']));
    $unidadesPresentacion=validar($data['unidadesPresentacion']);
    $costoPresentacion=validar($data['costoPresentacion']);
    $stock=validar($data['stock']);
    $color=strtoupper(validar($data['color']));
    $costoUnitario=validar($data['costoUnitario']);
    
    $sql="INSERT INTO insumos(proveedores_idproveedor,categoria,nombre,detalle,unidadMedida,presentacion,costoPresentacion,unidadesPresentacion,costoUnitario) VALUES (?,?,?,?,?,?,?,?,?)";
    $sql_insert=$pdo->prepare($sql);
    $sql_insert->execute(array($proveedor,$categoria,$nombre,$detalle,$unidadMedida,$presentacion,$costoPresentacion,$unidadesPresentacion,$costoUnitario));
    if($sql_insert){
        $sql="SELECT MAX(idinsumos) AS id FROM insumos";
        $insumo=$pdo->prepare($sql);
        $insumo->execute();
        $insumo=$insumo->fetch();
        $id=$insumo['id'];
        if($codInsumo===null){
            $codInsumo=$id;
        }
        $sql="UPDATE insumos SET codigoInsumo = ? WHERE idinsumos=?";
        $update_sql=$pdo->prepare($sql);
        $update_sql->execute(array($codInsumo,$id));
        if($update_sql){
            $sql="INSERT INTO colores(color,stock,insumos_idinsumos) VALUES (?,?,?)";
            $sql_insertColor=$pdo->prepare($sql);
            $sql_insertColor->execute(array($color,$stock,$id));
            if($sql_insertColor){
                $response = ["Insumo" => $nombre , "Estado" => true];
            }
        }
    }else{
        $response = ["Insumo" => $nombre , "Estado" => false];
    }

    echo json_encode($response);
?>