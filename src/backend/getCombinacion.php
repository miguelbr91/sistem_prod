<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $idproductos=$data['parametro'];
    $response=[];

    $sql="SELECT * FROM vw_combinaciones_prod where productos_idproductos=?";
    $sql_get=$pdo->prepare($sql);
    $sql_get->execute(array($idproductos));

    $response=$sql_get->fetchAll();

    echo json_encode($response);
?>