<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    include_once 'validarData.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    // Insertar Color
    $idInsumo=$data['insumos_idinsumos'];
    $stock=$data['stock'];
    $color=$data['color'];
    
    $sql="INSERT INTO colores(color,stock,insumos_idinsumos) VALUES (?,?,?)";
    $sql_insertColor=$pdo->prepare($sql);
    $sql_insertColor->execute(array($color,$stock,$idInsumo));
    if($sql_insertColor){
        $response = ["Color" => $color , "Estado" => true];
    }else{
        $response = ["Color" => $color , "Estado" => false];
    }

    echo json_encode($response);
?>