<?php
session_start();
$usr = $_SESSION['usuario'];
$year = $_GET['year'];
if(!empty($usr)){
    require('../backend/conn.php');
    require('fpdf.php');

    if(!empty($year)){
        $sql="SELECT categoria,nombre,precioMinorista FROM vw_productos WHERE `year`=$year ORDER BY categoria";    
    }else{
        $sql="SELECT categoria,nombre,precioMinorista FROM vw_productos ORDER BY categoria";
    }
    $gsent=$pdo->prepare($sql);
    $gsent->execute();
    $row=$gsent->fetchAll();
    $long=59;


    class PDF extends FPDF
    {
    // Cabecera de página
    function Header()
    {
        //Fecha Actual
        require('../backend/conn.php');
        $fecha=date('d/m/Y');
        $year = $_GET['year'];
        if(!empty($year)){
            $sql="SELECT categoria,nombre,precioMinorista FROM vw_productos WHERE `year`=$year ORDER BY categoria";    
        }else{
            $sql="SELECT categoria,nombre,precioMinorista FROM vw_productos ORDER BY categoria";
        }
        $gsent=$pdo->prepare($sql);
        $gsent->execute();
        $row=$gsent->fetchAll();
        $long=59;
        $this->SetFont('Arial','',9);
        $this->Cell(200,6,'Fecha: '.$fecha,0,1,'R');
        // Logo
        $this->Image('../img/logo-flor-sin-fondo.png',10,8,23);
        // Arial bold 16
        $this->SetFont('Arial','B',16);
        // Movernos a la derecha
        $this->Cell(70);
        // Título
        if(!empty($year)){
            $this->Cell(50,10,'Lista de Precios: Productos del '.$year ,0,0,'C');
        }else{
            $this->Cell(50,10,'Lista de Precios Completa ',0,0,'C');
        }
        // Salto de línea
        $this->Ln(15);

        //Cabecera de tabla
        $this->SetFont('Arial','B',10);
        if($long<sizeof($row)){
            $this->Cell(24,6, 'Categoria', 1, 0, 'C');
            $this->Cell(60,6, 'Nombre de Producto', 1, 0, 'L');
            $this->Cell(16,6, 'Precio', 1, 0, 'C');
            $this->Cell(24,6, 'Categoria', 1, 0, 'C');
            $this->Cell(60,6, 'Nombre de Producto', 1, 0, 'L');
            $this->Cell(16,6, 'Precio', 1, 1, 'C');
        }else{
            $this->Cell(24,6, 'Categoria', 1, 0, 'C');
            $this->Cell(60,6, 'Nombre de Producto', 1, 0, 'L');
            $this->Cell(16,6, 'Precio', 1, 1, 'C');
        }
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,utf8_decode('Página ').$this->PageNo().'/{nb}',0,0,'C');
    }
    }

    $pdf=new PDF('P','mm','Legal');
    $pdf->AliasNbPages();
    $pdf->AddPage();
    
    
    //Cuerpo de tabla
    $pdf->SetFont('Arial','',9);
    for ($i=0; $i < $long; $i++) { 
        # code...
        if(!empty($row[$i])){
            $pdf->Cell(24,5, $row[$i]['categoria'], 1, 0, 'C');
            $pdf->Cell(60,5, $row[$i]['nombre'], 1, 0, 'L');
            //$pdf->Cell(20,5, '$ '.$row[$i]['precioMinorista'], 1, 1, 'C');
            if(($long+$i)<sizeof($row)){
                $pdf->Cell(16,5, '$ '.$row[$i]['precioMinorista'], 1, 0, 'C');
                $pdf->Cell(24,5, $row[$i+$long]['categoria'], 1, 0, 'C');
                $pdf->Cell(60,5, $row[$i+$long]['nombre'], 1, 0, 'L');
                $pdf->Cell(16,5, '$ '.$row[$i+$long]['precioMinorista'], 1, 1, 'C');
            }else{
                $pdf->Cell(16,5, '$ '.$row[$i]['precioMinorista'], 1, 1, 'C');
            }
        }
    }
    if(sizeof($row)>(2*$long)){
        for ($i=2*$long; $i < 3*$long; $i++) { 
            # code...
            if(!empty($row[$i])){
                $pdf->Cell(24,5, $row[$i]['categoria'], 1, 0, 'C');
                $pdf->Cell(60,5, $row[$i]['nombre'], 1, 0, 'L');
                //$pdf->Cell(20,5, '$ '.$row[$i]['precioMinorista'], 1, 1, 'C');
                if(($long+$i)<sizeof($row)){
                    $pdf->Cell(16,5, '$ '.$row[$i]['precioMinorista'], 1, 0, 'C');
                    $pdf->Cell(24,5, $row[$i+(3*$long)]['categoria'], 1, 0, 'C');
                    $pdf->Cell(60,5, $row[$i+(3*$long)]['nombre'], 1, 0, 'L');
                    $pdf->Cell(16,5, '$ '.$row[$i+(3*$long)]['precioMinorista'], 1, 1, 'C');
                }else{
                    $pdf->Cell(16,5, '$ '.$row[$i]['precioMinorista'], 1, 1, 'C');
                }
            }
        }
    }
    $pdf->Output();
}else{
    header("Location: http://localhost:3000/src/");
}



?>