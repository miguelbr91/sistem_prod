new Vue({
    el: '#app',
    data:{
        insumos: [],
        proveedores: [],
        newInsumo: {},
        newColor:{},
        editarDatoInsumo: {},
        idProveedor:'',
        parametro: null,
        idColorEliminar: {},
        categorias:[],
        listCat:[],
        newCategoria:false,
        color:false,
        listColor:[],
        loader:false,
        newProveedor: {
            "nombre": '',
            "nombreContacto": '',
            "mail": '',
            "web": '',
            "direccion": '',
            "ciudad": '',
            "provincia": '',
            "comentario": '',
            "telefono": '',
            "celular": ''
        },
        updateStock:'',
        stockActual: 0,
    },
    methods: {
        addInsumo(){
            // calculo del costo unitario
            this.newInsumo.costoUnitario = Math.round((this.newInsumo.costoPresentacion/this.newInsumo.unidadesPresentacion)*100)/100
            this.newInsumo.stock=this.newInsumo.unidadesPresentacion*this.newInsumo.cantidad
            if((this.newInsumo.codigoInsumo===undefined)||(this.newInsumo.codigoInsumo==='')){
                this.newInsumo.codigoInsumo=null
            }
            
            console.log('Nuevo Insumo',this.newInsumo)
            let data=JSON.stringify(this.newInsumo)
            axios.post('backend/insertInsumo.php', data)
            .then(resp=>{
                console.log(resp.data)

                let alerta=document.getElementById('alert-insumo')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Insumo ${resp.data.Insumo} registrado correctamente</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Error! El insumo ${resp.data.Insumo} no se ha podido registrar</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.newInsumo = {}
            this.getInsumos()
            this.wNewInsumo(false)
            this.getCategorias()
        },
        getInsumos(param){
            let val = param
            if(val===undefined){
                val=null
            }
            let data = { "parametro" : val};
            axios.post('backend/getInsumos.php', data)
            .then(resp=>{
                console.log(resp.data)
                this.insumos=resp.data;
                for (let i = 0; i < this.insumos.length; i++) {
                    let date = new Date(this.insumos[i].fechaActualizacion)
                    this.insumos[i].fechaActualizacion=date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()
                }
            })
            .catch(e=>{
                console.log(e)
            })
        },
        wNewInsumo(state){
            const wni=document.getElementById('newInsumo')
            if(state){
                wni.classList.remove('d-none')
                wni.classList.add('dis-block')
            }else{
                wni.classList.remove('dis-block')
                wni.classList.add('d-none')
                this.newInsumo={}
            }
        },
        wEditInsumo(state){
            const wni=document.getElementById('editInsumo')
            if(state){
                wni.classList.remove('d-none')
                wni.classList.add('dis-block')
            }else{
                wni.classList.remove('dis-block')
                wni.classList.add('d-none')
                this.editarDatoInsumo={}
            }
        },
        getProveedor(){
            data={"parametro":null}
            axios.post('backend/getProveedores.php', data)
            .then(resp=>{
                console.log(resp.data)
                this.proveedores=resp.data;
            })
            .catch(e=>{
                console.log(e)
            })
        },
        editarInsumo(insumo){
            this.backToTop()
            this.wEditInsumo(true)
            this.editarDatoInsumo=insumo
            console.log(this.editarDatoInsumo)
        },
        addStock(insumo){
            this.updateStock = insumo
            this.stockActual = parseFloat(this.updateStock.stock)
            this.updateStock.cantidad = 0
            console.log('Sumar stock', this.updateStock)
        },
        updateNewStock(){
            this.updateStock.stock=this.stockActual+this.updateStock.cantidad*this.updateStock.unidadesPresentacion
        },
        confirmUpdateStock(){
            console.log('Nuevo Stock', this.updateStock)
            let data = {
                "idcolores": this.updateStock.idcolores,
                "stock": this.updateStock.stock
            }
            console.log(data)
            axios.post('backend/updateStock.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-insumo')
                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
        },
        guardarCambiosInsumo(){
            this.editarDatoInsumo.costoUnitario = Math.round((this.editarDatoInsumo.costoPresentacion/this.editarDatoInsumo.unidadesPresentacion)*100)/100
            if((this.editarDatoInsumo.codigoInsumo===undefined)||(this.editarDatoInsumo.codigoInsumo==='')){
                this.editarDatoInsumo.codigoInsumo=null
            }
            console.log('confirmar cambios')
            console.log(this.editarDatoInsumo)
            let data=JSON.stringify(this.editarDatoInsumo)
            axios.post('backend/editarInsumo.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-insumo')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>${resp.data.Data}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>${resp.data.Data}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.wEditInsumo(false)
            this.getInsumos()
        },
        addColor(idInsumo,unidades){
            this.loader=true
            this.newColor.insumos_idinsumos=idInsumo
            this.newColor.unidadesPresentacion=unidades
            console.log('Nuevo Color: '+idInsumo)
            console.log(this.newColor)
            setTimeout(()=>{this.loader=false},100)
        },
        confirmAddColor(){
            if(this.newColor.color==='' || this.newColor.color===undefined){
                console.log('error! agregue un color')
            }else{
                this.newColor.stock=this.newColor.cantidad*this.newColor.unidadesPresentacion
                console.log('Agrego nuevo color')
                console.log(this.newColor)      
                let data = JSON.stringify(this.newColor)
                axios.post('backend/insertColor.php', data)
                .then(resp=>{
                    console.log(resp.data)
    
                    let alerta=document.getElementById('alert-insumo')
    
                    if(resp.data.Estado===true){
                        alerta.innerHTML=`
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>Nuevo Color ${resp.data.Color} registrado correctamente</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        `
                    }else{
                        alerta.innerHTML=`
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Error! El Color ${resp.data.Color} no se ha podido registrar</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        `
                    }
                })
                .catch(e=>{
                    console.log(e)
                })
            }
            this.newColor = {}
            this.getInsumos()
        },
        deleteInsumo(idColor,idInsumo){
            this.idColorEliminar.idcolores=idColor;
            this.idColorEliminar.insumos_idinsumos=idInsumo;
            console.log('Borrar Insumo')
            console.log(this.idColorEliminar)
        },
        confirmarInsumoEliminado(){
            let data=JSON.stringify(this.idColorEliminar)
            axios.post('backend/deleteInsumo.php', data)
            .then(resp => {
                console.log(resp.data)

                let alerta=document.getElementById('alert-insumo')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>${resp.data.Data}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>${resp.data.Data}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.getInsumos()
        },
        backToTop(){
            $('html,body').stop().animate({
              scrollTop: 0
            }, 'slow', 'swing');
        },
        getCategorias(){
            axios.post('backend/getCategories.php')
            .then(resp=>{
                this.categorias=resp.data
                let cat=resp.data
                for (let index = 0; index < cat.length; index++) {
                    let element = cat[index];
                    this.listCat[index] = element.categoria
                }
                console.log(this.categorias)
                console.log(this.listCat)
            })
        },
        setBackgraund(cat){
            let fondos=this.categorias
            let myCat=cat
            for (let i = 0; i < fondos.length; i++) {
                const element = fondos[i];
                if(element.categoria===myCat){
                    return element.bg
                }
            }
        },
        getColor(){
            axios.post('backend/getColor.php')
            .then(resp=>{
                let data=resp.data
                for (let i = 0; i < data.length; i++) {
                    this.listColor[i] = data[i].color;
                }
                console.log(this.listColor)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        addProveedor(){
            console.log(this.newProveedor)
            let data=this.newProveedor
            axios.post('backend/insertProveedor.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-insumo')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Correcto!</strong> Proveedor <strong>${resp.data.Proveedor}</strong> registrado correctamente
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Error! </strong>El proveedor <strong>${resp.data.Proveedor}</strong> no se ha podido registrar
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.getProveedor()
        }
    },
    created() {
        // cargar datos de proveedores de DB
        this.getProveedor()
        this.getInsumos()

        let btnEliminarInsumo = document.getElementById('btnEliminarInsumo')
        btnEliminarInsumo.addEventListener('click', () => {
            this.confirmarInsumoEliminado()
        })
        this.getCategorias()
        this.getColor()
    },
    mounted() {
        // Filtrar busqueda insumo al presionar teclado
        let keyupSearch = document.getElementById('buscarInsumo')
        keyupSearch.addEventListener('keyup', ()=>{
            this.getInsumos(this.parametro)
        })

    },
    updated() {
        //console.log('se actualizo')
    },
})