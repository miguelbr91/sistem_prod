new Vue({
    el: "#app",
    data:{
        titulo: 'Test Login',
        user:{
            name:'',
            password:''
        }
    },
    methods: {
        login(){
            let user = {
                name : this.user.name,
                pass : this.user.password
            } 
            axios.post('../../backend/login.php', user)
            .then(res=>{
                if(res.data.error===undefined){
                    console.log(res.data)
                }else{
                    let alertSesion = document.getElementById('alertSesion');
                    alertSesion.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Error!</strong> ${res.data.error}.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>`
                }
            })
            .catch(e=>{
                console.log(e)
            })
        }
    },
    created() {
    },
    mounted() {
    },
    beforeUpdate() {
    },
    updated() {
    },
})