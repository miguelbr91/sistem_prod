new Vue({
    el: '#app',
    data:{
        title: 'Configuraciones de Sistema',
        porcentajeGananciaAros: 0,
        porcentajeGananciaCollares: 0,
        porcentajeGananciaPulseras: 0,
        precioHoraAros:0,
        precioHoraCollares:0,
        precioHoraPulseras:0
    },
    methods: {
        getSetting(){
            axios.get('backend/getSetting.php')
            .then(resp=>{
                console.log(resp.data)
                let setting = resp.data
                this.porcentajeGananciaAros=parseFloat(setting[0].porcentajeGanancia)*100
                this.porcentajeGananciaCollares=parseFloat(setting[1].porcentajeGanancia)*100
                this.porcentajeGananciaPulseras=parseFloat(setting[2].porcentajeGanancia)*100
                this.precioHoraAros=parseFloat(setting[0].precioHoras)
                this.precioHoraCollares=parseFloat(setting[1].precioHoras)
                this.precioHoraPulseras=parseFloat(setting[2].precioHoras)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        modificar(input){
            let item=document.getElementById(input)
            item.disabled=false
        },
        guardarConf(){
            console.log('configuracion guardada')
            let data={
                "porcentajeGananciaAros": (this.porcentajeGananciaAros/100),
                "precioHoraAros":this.precioHoraAros,
                "porcentajeGananciaCollares": (this.porcentajeGananciaCollares/100),
                "precioHoraCollares":this.precioHoraCollares,
                "porcentajeGananciaPulseras": (this.porcentajeGananciaPulseras/100),
                "precioHoraPulseras":this.precioHoraPulseras
            }
            axios.post('backend/updateSetting.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-setting')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
                
                let gananciaAros=document.getElementById('ganancia-aros')
                let gananciaCollares=document.getElementById('ganancia-collares')
                let gananciaPulseras=document.getElementById('ganancia-pulseras')
                let horas=document.getElementById('precioHora')
                gananciaAros.disabled=true
                gananciaCollares.disabled=true
                gananciaPulseras.disabled=true
                precioHoraAros.disabled=true
                precioHoraCollares.disabled=true
                precioHoraPulseras.disabled=true
                this.getSetting()
            })
            .catch(e=>{
                console.log(e)
            })
        },
    },
    created() {
        this.getSetting()
    },
})