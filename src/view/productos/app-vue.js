new Vue({
    el: '#app',
    data:{
        categoriasProducto: [
            {
                "idcategorias": 1,
                "nombre": 'AROS',
            },
            {
                "idcategorias": 2,
                "nombre": 'COLLARES',
            },
            {
                "idcategorias": 3,
                "nombre": 'PULSERAS',
            },
        ],
        producto: {
            "nombre": '',
            "imagen": 'img/no-preview-available.png',
            "categoria": '',
            "horasTrabajo": '',
            "precioSugerido": 0,
            "precioMinorista": 0,
            "precioMayorista": 0,
            "year": null,
            "insumos": [],
            "packaging": [],
            "combinaciones": []
        },
        setting: null,
        porcentajeGanancia: 0,
        precioHora: 0,
        costoManoObra: 0,
        costoProducto: 0,
        busqInsumo: '',
        busqPack: '',
        listInsumos: [],
        categorias:[],
        listCat:[],
        cargarInsumo: false,
        insumo: '',
        listaPack: '',
        costoTotalInsumos:0,
        costoTotalPack:0,
            //variables combinaciones de color
            checkListaCombinaciones: true,
            listaCombinaciones: [],
            insumosComb: [],
            listColor: [],
            miCombinacion:{
                "primario":{
                    "color":null,
                    "insumos_idinsumos": []
                },
                "secundario":{
                    "color":null,
                    "insumos_idinsumos": []
                }
            },
            listInsumosTipComb:[],
            indexDeleteComb:null,
            indexEditComb:null,
            //fin variables combinaciones de color
    },
    methods: {
        async fileSelected(event){
            let preview=document.getElementById('imgPreview')
            let file = event.target.files[0]

            // let toDataURL = url => fetch(url)
            //     .then(response => response.blob())
            //     .then(blob => new Promise((resolve, reject) => {
            //         let reader = new FileReader()
            //         reader.onloadend = () => resolve(reader.result)
            //         reader.onerror = reject
            //         reader.readAsDataURL(blob)
            //     }))
            // toDataURL(URL.createObjectURL(file))
            // .then(dataUrl => {
            //     this.producto.imagen = dataUrl
            // })


            let fd = new FormData()
            fd.append('image',file,file.name)
            await axios.post('backend/resize.php',fd)
            .then(resp=>{
                console.log(resp.data)
                let dataUrl = resp.data.blob
                this.producto.imagen = dataUrl
            })
            .catch(e=>{
                console.log(e)
            })

            let reader  = new FileReader();

            reader.onloadend = function () {
                preview.src = reader.result;
            }

            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        },
        getSetting(){
            axios.get('backend/getSetting.php')
            .then(resp=>{
                this.setting = resp.data
                console.log(this.setting)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        settingCategoria(){
            let cat = this.producto.categoria
            switch (cat) {
                case 1:
                    this.porcentajeGanancia=parseFloat(this.setting[0].porcentajeGanancia)
                    this.precioHora=parseFloat(this.setting[0].precioHoras)
                    break;
                case 2:
                        this.porcentajeGanancia=parseFloat(this.setting[1].porcentajeGanancia)
                        this.precioHora=parseFloat(this.setting[1].precioHoras)
                        break;
                case 3:
                        this.porcentajeGanancia=parseFloat(this.setting[2].porcentajeGanancia)
                        this.precioHora=parseFloat(this.setting[2].precioHoras)
                        break;
            }
            this.precioSugerido()
            console.log(this.porcentajeGanancia)
            console.log(this.precioHora)
        },
        getInsumos(param){
            if(param!=''){
                let data = {"parametro": param}
                axios.post('backend/getInsumosProducto.php', data)
                .then(resp=>{
                    console.log(resp.data)
                    this.listInsumos=resp.data
                })
                .catch(e=>{
                    console.log(e)
                })
            }else{
                this.listInsumos= []
            }
        },
        setBackgraund(cat){
            let fondos=this.categorias
            let myCat=cat
            for (let i = 0; i < fondos.length; i++) {
                const element = fondos[i];
                if(element.categoria===myCat){
                    return element.bg
                }
            }
        },
        getCategorias(){
            axios.post('backend/getCategories.php')
            .then(resp=>{
                this.categorias=resp.data
                let cat=resp.data
                for (let index = 0; index < cat.length; index++) {
                    this.listCat[index] = cat[index].categoria
                }
                console.log(this.categorias)
                console.log(this.listCat)
            })
        },
        selectInsumo(insumo){
            let newInsumo = insumo
            newInsumo.cantidadInsumo = 0
            console.log(insumo)
            this.listInsumos=[insumo]
            this.cargarInsumo=true
            this.insumo=newInsumo
            this.busqInsumo=''
        },
        quitarSeleccion(){
            this.listInsumos=[]
            this.cargarInsumo=false
            this.insumo=''
        },
        addInsumo(){
            console.log(this.insumo)
            this.producto.insumos.push(this.insumo)
            this.costoProducto+=this.insumo.cantidadInsumo*this.insumo.costoUnitario
            this.costoTotalInsumos+=Math.round((this.insumo.cantidadInsumo*this.insumo.costoUnitario)*100)/100
            this.precioSugerido()
            console.log(this.costoProducto)
            this.insumo=''
            this.listInsumos=[]
            this.cargarInsumo=false
            console.log('Insumo agregado a producto')
            console.log(this.producto)
        },
        quitarInsumo(index){
            this.costoProducto-=this.producto.insumos[index].cantidadInsumo*this.producto.insumos[index].costoUnitario
            this.costoTotalInsumos-=Math.round((this.producto.insumos[index].cantidadInsumo*this.producto.insumos[index].costoUnitario)*100)/100
            this.precioSugerido()
            console.log(this.costoProducto)
            this.producto.insumos.splice(index,1)
            console.log(this.producto.insumos)
        },
        getPack(param){
            let val=param
            if(val===undefined){
                val=null
            }
            let data={"parametro":val}
            axios.post('backend/getPackaging.php', data)
            .then(resp=>{
                console.log(resp.data)
                this.listaPack=resp.data
            })
            .catch(e=>{
                console.log(e)
            })
        },
        addPack(pack, index){
            this.costoProducto+=Math.round((parseFloat(pack.precioUnitario))*100)/100
            this.costoTotalPack+=Math.round((parseFloat(pack.precioUnitario))*100)/100
            this.precioSugerido()
            console.log(this.costoProducto)
            this.producto.packaging.push(pack)
            this.listaPack.splice(index,1)
        },
        quitarPack(pack,index){
            this.costoProducto-=Math.round((parseFloat(pack.precioUnitario))*100)/100
            this.costoTotalPack-=Math.round((parseFloat(pack.precioUnitario))*100)/100
            this.precioSugerido()
            console.log(this.costoProducto)
            this.listaPack.push(pack)
            this.producto.packaging.splice(index,1)
        },
        addManoObra(){
            this.costoManoObra=this.producto.horasTrabajo*this.precioHora
            this.precioSugerido()
        },
        precioSugerido(){
            this.producto.precioSugerido=Math.round(((this.costoProducto+this.costoManoObra)*this.porcentajeGanancia)*100)/100
        },
        precioMayorista(){
            this.producto.precioMayorista=Math.round((this.producto.precioMinorista*0.6)*100)/100
        },
        backToTop(){
            $('html,body').stop().animate({
              scrollTop: 0
            }, 'slow', 'swing');
        },
        addProducto(){
            console.log('Nuevo Prodcuto agregado!')
            this.producto.combinaciones = this.listaCombinaciones
            let data = this.producto
            console.log(data)
            axios.post('backend/insertProducto.php',data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-producto')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.producto={
                "nombre": '',
                "imagen": 'img/no-preview-available.png',
                "categoria": '',
                "horasTrabajo": '',
                "precioSugerido": 0,
                "precioMinorista": 0,
                "precioMayorista": 0,
                "year": null,
                "insumos": [],
                "packaging": [],
                "combinaciones": []
            }
            this.costoManoObra=0
            this.costoProducto=0
            this.costoTotalInsumos=0
            this.costoTotalPack=0
            let preview=document.getElementById('imgPreview')
            preview.src=this.producto.imagen
            this.getPack()
            this.backToTop()
        },
            // metodos para la carga de combinaciones
            checkCombinaciones(){
                let comb=this.listaCombinaciones
                if(comb.length>0){
                    this.checkListaCombinaciones=true
                }else{
                    this.checkListaCombinaciones=false
                }
            },
            cargarCombInsumos(){
                this.insumosComb = []
                this.listInsumosTipComb = []
                let insumos = this.producto.insumos.filter(item => (item.categoria === 'CRISTALES') || (item.categoria === 'PIEDRAS'))
                this.insumosComb = insumos
                for (let i = 0; i < this.insumosComb.length; i++) {
                    let object = {
                        "insumos_idinsumos":this.insumosComb[i].idinsumos,
                        "tipo":0
                    }
                    this.listInsumosTipComb.push(object)
                }
                console.log(this.insumosComb)
                console.log(this.listInsumosTipComb)
            },
            getColor(){
                axios.post('backend/getColor.php')
                .then(resp=>{
                    let data=resp.data
                    for (let i = 0; i < data.length; i++) {
                        this.listColor[i] = {
                            "color":data[i].color
                        }
                    }
                    console.log(this.listColor)
                })
                .catch(e=>{
                    console.log(e)
                })
            },
            removeItemFromArr( arr, item ){
                let i = arr.indexOf( item )

                if ( i !== -1 ) {
                    arr.splice( i, 1 )
                }
            },
            cargarInsTipoComb(id){
                let insumo = this.listInsumosTipComb.filter(item => item.insumos_idinsumos === id)
                console.log(insumo)
                this.removeItemFromArr(this.miCombinacion.primario.insumos_idinsumos,id)
                this.removeItemFromArr(this.miCombinacion.secundario.insumos_idinsumos,id)
                switch (insumo[0].tipo) {
                    case 1:
                        this.miCombinacion.primario.insumos_idinsumos.push(id)
                        break;
                    case 2:
                        this.miCombinacion.secundario.insumos_idinsumos.push(id)
                        break;
                }
                console.log(this.miCombinacion.primario.insumos_idinsumos)
                console.log(this.miCombinacion.secundario.insumos_idinsumos)
            },
            addCombinacion(){
                this.listaCombinaciones.push(this.miCombinacion)
                this.miCombinacion = {
                    "primario":{
                        "color":null,
                        "insumos_idinsumos": []
                    },
                    "secundario":{
                        "color":null,
                        "insumos_idinsumos": []
                    }
                }
                for (let i = 0; i < this.listInsumosTipComb.length; i++) {
                    this.listInsumosTipComb[i].tipo=0
                }
            },
            editarComb(comb, index){
                console.log('editar',comb)
                this.miCombinacion=comb
                this.indexEditComb=index
                let primario = comb.primario.insumos_idinsumos
                let secundario = comb.secundario.insumos_idinsumos
                for (let i = 0; i < primario.length; i++) {
                    let tipo = this.listInsumosTipComb.filter(item => item.insumos_idinsumos === primario[i])
                    tipo[0].tipo=1
                }
                for (let i = 0; i < secundario.length; i++) {
                    let tipo = this.listInsumosTipComb.filter(item => item.insumos_idinsumos === secundario[i])
                    tipo[0].tipo=2
                }
                console.log(this.listInsumosTipComb)
            },
            addModifCombinacion(){
                let index = this.indexEditComb
                this.listaCombinaciones.splice(index,1,this.miCombinacion)
                
                //Resetear variables
                this.miCombinacion = {
                    "primario":{
                        "color":null,
                        "insumos_idinsumos": []
                    },
                    "secundario":{
                        "color":null,
                        "insumos_idinsumos": []
                    }
                }
                for (let i = 0; i < this.listInsumosTipComb.length; i++) {
                    this.listInsumosTipComb[i].tipo=0
                }
                this.indexEditComb=null
            },
            cancelModifCombinacion(){
                this.indexEditComb=null
                this.cancelarCombinacion()
            },
            cancelarCombinacion(){
                this.miCombinacion = {
                    "primario":{
                        "color":null,
                        "insumos_idinsumos": []
                    },
                    "secundario":{
                        "color":null,
                        "insumos_idinsumos": []
                    }
                }
                for (let i = 0; i < this.listInsumosTipComb.length; i++) {
                    this.listInsumosTipComb[i].tipo=0
                }
            },
            borrarComb(comb,index){
                console.log('borrar',comb, 'index:', index)
                this.miCombinacion=comb
                this.indexDeleteComb=index
            },
            deleteComb(){
                if(this.indexDeleteComb!==null){
                    this.listaCombinaciones.splice( this.indexDeleteComb, 1 )
                }
            },
            cancelDeleteComb(){
                this.indexDeleteComb=null
            }
            // fin metodos para la carga de combinaciones
    },
    created() {
        this.getCategorias()
        this.getPack()
        this.getSetting()
        this.getColor()
    },
    mounted(){
        let keyupBusqInsumo=document.getElementById('search-insumo')
        keyupBusqInsumo.addEventListener('keyup', ()=>{
            this.getInsumos(this.busqInsumo)
        })

        let keyupManoObra=document.getElementById('horasTrabajoProducto')
        keyupManoObra.addEventListener('keyup',()=>{
            this.addManoObra()
        })

        let keyupPrecioMin=document.getElementById('precioMinoristaProducto')
        keyupPrecioMin.addEventListener('keyup',()=>{
            this.precioMayorista()
        })
    },
    updated() {
        this.checkCombinaciones();
    },
})

