new Vue({
    el: '#app',
    data:{
        user:{}
    },
    methods: {
        async ingresar(){
            let btnLongin = document.getElementById('btnLogin')
            btnLongin.innerHTML=`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>Loading...`
            console.log('ingresar como', this.user)
            let data=JSON.stringify(this.user)
            await axios.post('backend/login.php', data)
            .then(res=>{
                if(res.data.error===undefined){
                    console.log(res.data)
                    location.reload()
                }else{
                    let alertSesion = document.getElementById('alertSesion');
                    alertSesion.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Error!</strong> ${res.data.error}.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>`
                }
            })
            .catch(e=>{
                console.log(e)
            })
            btnLongin.innerHTML=`Ingresar`
        }
    }
})