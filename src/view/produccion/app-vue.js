new Vue({
    el: "#app",
    data:{
        NUM_RESULTS: 5, // Numero de resultados por página
        pag: 1, // Página inicial
        paginas: 1,
        categoriaProducto: null,
        cargarProducto:false,
        saveProduction:false,
        listaProduccion:[],
        listProductos:[],
        listaColores:[
            {
                "orden":0,
                "color": 'NIQUEL', 
                "cantidad": 0
            },
            {
                "orden":1,
                "color": 'PERLA', 
                "cantidad": 0
            },
            {
                "orden":2,
                "color":'AMBAR',
                "cantidad":0
            },
            {
                "orden":3,
                "color":'AQUA',
                "cantidad":0,
            },
            {
                "orden":4,
                "color":'AZUL',
                "cantidad":0,
            },
            {
                "orden":5,
                "color":'BOREAL',
                "cantidad":0,
            },
            {
                "orden":6,
                "color":'HUMO',
                "cantidad":0,
            },
            {
                "orden":7,
                "color":'NEGRO',
                "cantidad":0,
            },
            {
                "orden":8,
                "color":'PELTRE',
                "cantidad":0,
            },
            {
                "orden":9,
                "color":'ROJO',
                "cantidad":0,
            },
            {
                "orden":10,
                "color":'UVA',
                "cantidad":0,
            },
            {
                "orden":11,
                "color":'VERDE',
                "cantidad":0,
            },
            {
                "orden":12,
                "color":'MULTICOLOR',
                "cantidad":0
            }
        ],
        years: [],
        meses: [
            {"nro":1,"nombre":'Enero'},
            {"nro":2,"nombre":'Febrero'},
            {"nro":3,"nombre":'Marzo'},
            {"nro":4,"nombre":'Abril'},
            {"nro":5,"nombre":'Mayo'},
            {"nro":6,"nombre":'Junio'},
            {"nro":7,"nombre":'Julio'},
            {"nro":8,"nombre":'Agosto'},
            {"nro":9,"nombre":'Septiembre'},
            {"nro":10,"nombre":'Octubre'},
            {"nro":11,"nombre":'Noviembre'},
            {"nro":12,"nombre":'Diciembre'},
        ],
        yearProduccion: null,
        mesProduccion: null,
        vistaProduction: [],
        vistaProductionProductos: [],
        productosFiltrados: [],
        idDeleteProduccion: null,
        listConfiguracionInsumosProducto: [],   // lista de configuraciones del producto
        confColor:['AMBAR','AQUA','AZUL','BOREAL','HUMO','NEGRO','PELTRE','ROJO','UVA','VERDE'],
        cantidadConfig:0,
        cantXproducir:0,
        totalConfig: false,
        insumosProduccion: {
            "produccion":'',
            "listaInsumos":null
        }
    },
    methods: {
        async listaProductos(){
            this.listProductos=[]
            let val = this.categoriaProducto
            let data = {"parametro": val}
            console.log(data)
            this.cargarProducto=true
            if(val!=null){
                await axios.post('backend/getProductos.php', data)
                .then(resp=>{
                    this.listProductos=resp.data
                    console.log(this.listProductos)
                    for (let i = 0; i < this.listProductos.length; i++) {
                        this.listProductos[i].colores=[]
                        for (let j = 0; j < this.listaColores.length; j++) {
                            this.listProductos[i].colores.push({"orden": this.listaColores[j].orden ,"color": this.listaColores[j].color ,"cantidad":0})
                        }
                    }
                    console.log(this.listProductos)
                    this.cargarProducto=false
                })
                .catch(e=>{
                    console.log(e)
                })
            }else{
                this.listProductos=[]
            }
        },
        sumProd(id){
            let total = 0
            for (let i = 0; i < this.listProductos[id].colores.length; i++) {
                total+=this.listProductos[id].colores[i].cantidad
            }
            console.log(total)
            this.listProductos[id].totalProducto=total
        },
        sumTotProd(item){
            let cont = 0;
            for (let i = 0; i < item.length; i++) {
                cont += parseInt(item[i].cantidad)
            }
            return cont
        },
        totalProducir(){
            let total = 0
            for (let i = 0; i < this.listProductos.length; i++) {
                if(this.listProductos[i].totalProducto!=undefined){
                    total+=this.listProductos[i].totalProducto;
                }
            }
            this.listProductos[0].cantidadProducir=total
        },
        async addProduccion(){
            this.totalProducir()
            this.listProductos[0].year=this.yearProduccion
            this.listProductos[0].mesNro=this.mesProduccion
            this.listProductos[0].mesNombre=this.meses[this.mesProduccion-1].nombre
            let data = this.listProductos
            console.log(data)
            this.saveProduction=true
            await axios.post('backend/insertProduccion.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-producto')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
                this.getProduction()
                this.saveProduction=false
            })
            .catch(e=>{
                console.log(e)
            })
            this.listProductos=[]
            this.yearProduccion=null
            this.mesProduccion=null
        },
        getProduction(){
            axios.get('backend/getProduction.php')
            .then(resp=>{
                this.listaProduccion=resp.data
                console.log(this.listaProduccion)
                for(let i=0;i<this.listaProduccion.length;i++){
                    let date = new Date(this.listaProduccion[i].fecha)
                    this.listaProduccion[i].fecha=date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()
                }
            })
            .catch(e=>{
                console.log(e)
            })
        },
        mostrarInsumos(idproduction){
            let data = {
                "parametro":idproduction
            }
            axios.post('backend/getInsumosProduccion.php', data)
            .then(resp=>{
                console.log(resp.data)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        configurarProducto(idproducto,cantidad,nombreProducto,index){
            console.log('Configurar Producto ID:', idproducto)
            console.log('Configurar Producto index:', index)
            this.datoProductoMulticolor.idproductos= idproducto
            this.datoProductoMulticolor.index= index
            this.datoProductoMulticolor.nombre= nombreProducto
            this.datoProductoMulticolor.cantidadProducir= cantidad
            this.cantXproducir=cantidad
            console.log('Dato Producto:', this.datoProductoMulticolor)
            this.getListaInsumosMulticolor(idproducto)
        },
        guardarConfiguracionProducto(){
            // guardar "this.configuracionInsumosProducto" en el array de lista de productos
            console.log('Lista de configuracion: ',this.listConfiguracionInsumosProducto)
            let index=this.datoProductoMulticolor.index
            let max=this.listProductos[index].colores.length-1
            this.listProductos[index].colores[max].config = this.listConfiguracionInsumosProducto
            console.log('Lista de insumos configurado producto: ',this.listProductos)
            this.datoProductoMulticolor= {
                "idproductos":null,
                "nombre": '',
                "cantidadProducir": 0,
                "cantidadConf": 0,
                "index":null
            }
            this.listConfiguracionInsumosProducto= []
        },
        addconfMulticolor(idproducto){
            this.datoProductoMulticolor.cantidadConf+=parseInt(this.cantidadConfig)
            this.cantXproducir=this.datoProductoMulticolor.cantidadProducir-this.datoProductoMulticolor.cantidadConf
            let max=this.listConfiguracionInsumosProducto.length-1
            this.listConfiguracionInsumosProducto[max].estado=true
            this.listConfiguracionInsumosProducto[max].cantidadProdConfig=parseInt(this.cantidadConfig)
            this.cantidadConf=0
            if(this.datoProductoMulticolor.cantidadConf<this.datoProductoMulticolor.cantidadProducir){
                this.getListaInsumosMulticolor(idproducto)
                this.totalConfig=false
            }            
            console.log(this.listConfiguracionInsumosProducto)
        },
        saveConfiguracionColor(insumo, lista){
            let max= insumo.combinacion.length-1
            insumo.combinacion[max].saveConfiguracionColor=true
            this.controlCantConfig(insumo)
            console.log(insumo)
            console.log('Listado de insuimos:',lista)
            this.controlTotalConfig(lista)
        },
        addConfiguracionColor(insumo){
            insumo.combinacion.push({
                "color":null,
                "cantidad":0,
                "saveConfiguracionColor":false
            })
        },
        controlCantConfig(insumo){
            let max= insumo.combinacion.length-1
            insumo.cantInsumoXconf-=insumo.combinacion[max].cantidad
        },
        controlTotalConfig(lista){
            let insXconf=0
            for (let i = 0; i < lista.length; i++) {
                insXconf+=lista[i].cantInsumoXconf
            }
            console.log('Total x configurar',insXconf)
            if(insXconf===0){
                this.totalConfig=true
            }
        },
        verConfiguracionProducto(producto){
            this.vistaConfMulticolor.nombre=producto.nombre
            this.vistaConfMulticolor.multicolor.cantidad=producto.colores[12].cantidad
            this.vistaConfMulticolor.multicolor.configuracion=producto.colores[12].configuracion
            console.log('configuracion de producto',this.vistaConfMulticolor)
        },
        async verProduccion(production,productionCategoria){
            // console.log(production)
            // console.log(productionCategoria)
            //this.vistaProduction=production
            this.vistaProduction.idproduction=production.idproduction
            this.vistaProduction.fecha=production.fecha
            this.vistaProduction.year=production.year
            this.vistaProduction.mesNombre=production.mesNombre
            this.vistaProduction.mesNro=production.mesNro
            this.vistaProduction.idCategoriaProduccion=productionCategoria.idCategoriaProduccion
            this.vistaProduction.nombreCategoria=productionCategoria.nombreCategoria
            let data={
                "idCategoriaProduccion":productionCategoria.idCategoriaProduccion
            }
            //console.log('data:',data)
            this.cargarProducto=true
            axios.post('backend/getProductosProduction.php',data)
            .then(resp=>{
                this.vistaProduction.productos = resp.data
                this.vistaProductionProductos = resp.data
                this.vistaProductionProductos = this.vistaProduction.productos.filter(item => this.sumTotProd(item.colores)>0)
                //console.log(this.vistaProduction)
                console.log(this.vistaProductionProductos)
                this.cargarProducto=false
            })
            .catch(e=>{
                console.log(e)
            })
        },
        deleteProduccion(productionCategoria){
            this.idDeleteProduccion=productionCategoria.idCategoriaProduccion
            console.log(this.idDeleteProduccion)
        },
        confirmarDeleteProduccion(){
            let data={
                "idCategoriaProduccion":this.idDeleteProduccion
            }
            axios.post('backend/deleteProduction.php',data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-producto')
                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
                this.deleteProduccion=null
                this.getProduction()
            })
            .catch(e=>{
                console.log(e)
            })
        },
        cancelDeleteProduccion(){
            this.idDeleteProduccion=null
            console.log(this.idDeleteProduccion)
        },
        addProdHecho(elto,idbtn1,idbtn2){
            console.log(elto)
            if(elto.realizado<elto.cantidad){
                elto.realizado++
                document.getElementById(idbtn1).disabled = false
            }else{
                document.getElementById(idbtn2).disabled = true
            }
            console.log(elto)
        },
        removeProdHecho(elto,idbtn1,idbtn2){
            if(elto.realizado>0){
                elto.realizado--
                document.getElementById(idbtn2).disabled = false
            }else{
                document.getElementById(idbtn1).disabled = true
            }
        },
        verInsumos(production,productionCategoria){
            this.insumosProduccion.produccion = productionCategoria
            this.insumosProduccion.produccion.year = production.year
            this.insumosProduccion.produccion.mesNombre = production.mesNombre
            let data = {
                "parametro": productionCategoria.idCategoriaProduccion
            }
            console.log(data)
            axios.post('backend/getInsumosProduction.php',data)
            .then(resp=>{
                console.log(resp.data)
                this.insumosProduccion.listaInsumos=resp.data
                this.insumosProduccion.listaInsumos = this.insumosProduccion.listaInsumos.sort(function (a, b) {
                    if (parseInt(a.codigoInsumo) > parseInt(b.codigoInsumo)) {
                      return 1;
                    }
                    if (parseInt(a.codigoInsumo) < parseInt(b.codigoInsumo)) {
                      return -1;
                    }
                    return 0;
                })
            })
            .catch(e=>{
                console.log(e)
            })
            console.log(this.insumosProduccion)
        },
        generarYears(){
            let fecha = new Date()
            let year = fecha.getFullYear()
            for (let i = 0; i < 5; i++) {
                this.years[i]=year++
            }
        },
        setBackgraund(index){
            if(index%4===0 || index===1 || index ===5 || index ===9){
                return 'bg-conf'
            }else{
                return 'bg-light'
            }
        },
        setBackgraund2(index){
            if(index%4===0 || index===3 || index ===7 || index ===11){
                return 'bg-conf'
            }else{
                return 'bg-light'
            }
        },
        async editarProduccion(produccion){
            let data = {
                "idproduction": produccion.idproduction,
                "idCategoriaProduccion": produccion.idCategoriaProduccion,
                "productos": produccion.productos
            }
            // console.log(data)
            this.saveProduction=true
            await axios.post('backend/updateProduction.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-producto')
                if(resp.data.Error===false){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
                this.getProduction()
                this.saveProduction=false
            })
            .catch(e=>{
                console.log(e)
            })
        }
    },
    created() {
        this.getProduction(0)
        this.listaColores=this.listaColores.sort()
        this.generarYears()
    },
    mounted() {
        let keySearch = document.getElementById('buscarProductos')
        keySearch.addEventListener('keyup',()=>{
            this.search(this.busqProducto)
        })
    },
    beforeUpdate() {
    },
    updated() {
        // let nuevaProduccionBody = document.getElementById('nuevaProduccionBody')
        // nuevaProduccionBody.addEventListener('scroll', ()=>{
        //     let optionsProduction = document.getElementById('optionsProduction')
        //     optionsProduction=optionsProduction.clientHeight
        //     console.log('Altura: ',optionsProduction)
        //     let alturaScroll=nuevaProduccionBody.scrollTop
        //     console.log('Scroll: ',alturaScroll)
        //     if(alturaScroll>optionsProduction){
        //         console.log('si')
        //     }else{
        //         console.log('no')
        //     }
        // })
    },
})