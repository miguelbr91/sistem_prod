new Vue({
    el: '#app',
    data:{
        proveedores: [],
        newProveedor: {
            "nombre": '',
            "nombreContacto": '',
            "mail": '',
            "web": '',
            "direccion": '',
            "ciudad": '',
            "provincia": '',
            "comentario": '',
            "telefono": '',
            "celular": ''
        },
        editarDatoProveedor: {},
        idProveedorEliminado: {},
        parametro: ''
    },
    methods: {
        addProveedor(){
            console.log('Nuevo Proveedor',this.newProveedor)
            
            let data = this.newProveedor

            axios.post('backend/insertProveedor.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-proveedor')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Correcto!</strong> Proveedor <strong>${resp.data.Proveedor}</strong> registrado correctamente
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Error! </strong>El proveedor <strong>${resp.data.Proveedor}</strong> no se ha podido registrar
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })

            this.newProveedor = {"nombre": '',"nombreContacto": '',"mail": '',"web": '',"direccion": '',"ciudad": '',"provincia": '',"comentario": '',"telefono": '',"celular": ''}
            this.wNewProveedor(false)
            this.updateData();
        },
        editarProveedor(proveedor){
            this.backToTop()
            this.wEditProveedor(true)
            this.editarDatoProveedor=proveedor
            console.log(this.editarDatoProveedor)
        },
        guardarCambiosProveedor(){
            console.log('confirmar cambios')
            let data=JSON.stringify(this.editarDatoProveedor)
            axios.post('backend/editarProveedor.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-proveedor')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>${resp.data.Data}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>${resp.data.Data}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.wEditProveedor(false)
            this.updateData()
        },
        borrarProveedor(id){
            console.log(id)
            this.idProveedorEliminado={"id":id};
        },
        confirmarProveedorEliminado(){
            console.log('Eliminado Proveedor:', this.idProveedorEliminado)
            let id=JSON.stringify(this.idProveedorEliminado)
            axios.post('backend/deleteProveedor.php', id)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-proveedor')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>${resp.data.Data}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>${resp.data.Data}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.idProveedorEliminado={}
            this.updateData()
            this.backToTop()
        },
        wEditProveedor(state){
            const wep=document.getElementById('editProveedor')
            if(state){
                wep.classList.remove('d-none')
                wep.classList.add('dis-block')
            }else{
                wep.classList.remove('dis-block')
                wep.classList.add('d-none')
            }
        },
        wNewProveedor(state){
            const wnp=document.getElementById('newProveedor')
            if(state){
                wnp.classList.remove('d-none')
                wnp.classList.add('dis-block')
            }else{
                wnp.classList.remove('dis-block')
                wnp.classList.add('d-none')
            }
        },
        updateData(param){
            let val=param
            if(val===undefined){
                val=null
            }
            let data = {"parametro":val}
            axios.post('backend/getProveedores.php', data)
            .then(resp=>{
                console.log(resp.data)
                this.proveedores=resp.data;
            })
            .catch(e=>{
                console.log(e)
            })
        },
        backToTop(){
            $('html,body').stop().animate({
              scrollTop: 0
            }, 'slow', 'swing');
        }
    },
    computed: {
        
    },
    created() {
        this.updateData();
        let btnDelete = document.getElementById('btnEliminarProveedor')
        btnDelete.addEventListener('click',()=>{
            console.log('Eliminar')
            this.confirmarProveedorEliminado()
        })
    },
    mounted() {
        let keyupSearch = document.getElementById('buscarProveedor')
        keyupSearch.addEventListener('keyup', ()=>{
            this.updateData(this.parametro)
        })
    },
})