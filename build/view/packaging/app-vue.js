new Vue({
    el: '#app',
    data:{
        pack: {
            "idpackaging": null,
            "nombre": null,
            "stock": null,
            "precioUnitario": null,
            "img": 'img/no-preview-available.png'
        },
        img:null,
        listaPack: [],
        deletePackID: null,
        searchPack: null,
    },
    methods: {
        addPack(){
            this.pack.img=this.img
            let data=this.pack
            axios.post('backend/insertPackaging.php',data)
            .then(resp=>{
                console.log('----------Response----------')
                console.log(resp.data)
                this.getPack()
            })
            .catch(e=>{
                console.log(e)
            })
            this.img='img/no-preview-available.png'
            this.pack= {"id": null,"nombre": null,"stock": null,"costo": null,"img": null}
            let preview=document.getElementById('imgPreview')
            preview.src='img/no-preview-available.png'
        },
        fileSelected(event){
            let preview=document.getElementById('imgPreview')
            let file = event.target.files[0]

            let toDataURL = url => fetch(url)
                .then(response => response.blob())
                .then(blob => new Promise((resolve, reject) => {
                    let reader = new FileReader()
                    reader.onloadend = () => resolve(reader.result)
                    reader.onerror = reject
                    reader.readAsDataURL(blob)
                }))
            toDataURL(URL.createObjectURL(file))
            .then(dataUrl => {
                this.img = dataUrl
            })



            let reader  = new FileReader();

            reader.onloadend = function () {
                preview.src = reader.result;
            }

            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        },
        getPack(param){
            let val=param
            if(val===undefined){
                val=null
            }
            let data={"parametro":val}
            axios.post('backend/getPackaging.php', data)
            .then(resp=>{
                console.log(resp.data)
                this.listaPack=resp.data
            })
            .catch(e=>{
                console.log(e)
            })
        },
        deletePack(pack){
            this.deletePackID=pack.idpackaging
        },
        confirmDelete(){
            console.log('Eliminar pack: '+this.deletePackID)
            let data={"idpackaging":this.deletePackID}
            axios.post('backend/deletePackaging.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-packaging')
                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>${resp.data.message}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>${resp.data.message}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.getPack()
        },
        editPack(item){
            this.pack=item
            let preview=document.getElementById('imgPreview')
            preview.src=this.pack.img
            this.img=this.pack.img
        }
    },
    created() {
        this.getPack()
    },
    mounted() {
        let keyupSearch=document.getElementById('buscarPackaging')
        keyupSearch.addEventListener('keyup',()=>{
            console.log(this.searchPack)
            this.getPack(this.searchPack)
        })
    },
})