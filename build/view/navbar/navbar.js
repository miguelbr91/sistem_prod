function navActive(item){
    switch(item){
        case 'insumos': $('#nav-insumos').addClass('active');$('#nav-productos').removeClass('active');$('#nav-proveedores').removeClass('active');$('#nav-packaging').removeClass('active');$('#nav-produccion').removeClass('active');$('#nav-setting').removeClass('active');break;
        case 'productos': $('#nav-insumos').removeClass('active');$('#nav-productos').addClass('active');$('#nav-proveedores').removeClass('active');$('#nav-packaging').removeClass('active');$('#nav-produccion').removeClass('active');$('#nav-setting').removeClass('active');break;
        case 'proveedores': $('#nav-insumos').removeClass('active');$('#nav-productos').removeClass('active');$('#nav-proveedores').addClass('active');$('#nav-packaging').removeClass('active');$('#nav-produccion').removeClass('active');$('#nav-setting').removeClass('active');break;
        case 'packaging': $('#nav-insumos').removeClass('active');$('#nav-productos').removeClass('active');$('#nav-proveedores').removeClass('active');$('#nav-packaging').addClass('active');$('#nav-produccion').removeClass('active');$('#nav-setting').removeClass('active');break;
        case 'produccion': $('#nav-insumos').removeClass('active');$('#nav-productos').removeClass('active');$('#nav-proveedores').removeClass('active');$('#nav-packaging').removeClass('active');$('#nav-produccion').addClass('active');$('#nav-setting').removeClass('active');break;
        case 'setting': $('#nav-insumos').removeClass('active');$('#nav-productos').removeClass('active');$('#nav-proveedores').removeClass('active');$('#nav-packaging').removeClass('active');$('#nav-produccion').removeClass('active');$('#nav-setting').addClass('active');break;
    }
}


$('#insumos').click(function(){
    $('#container-data').load('view/insumos/insumos.html');
    navActive('insumos');
});
$('#nuevo-productos').click(function(){
    $('#container-data').load('view/productos/nuevo-productos.html');
    navActive('productos');
});
$('#listar-productos').click(function(){
    $('#container-data').load('view/productos/listar-productos.html');
    navActive('productos');
});
$('#proveedores').click(function(){
    $('#container-data').load('view/proveedores/proveedores.html');
    navActive('proveedores');
});
$('#packaging').click(function(){
    $('#container-data').load('view/packaging/packaging.html');
    navActive('packaging');
});
$('#produccion').click(function(){
    $('#container-data').load('view/produccion/produccion.html');
    navActive('produccion');
});
$('#setting').click(function(){
    $('#container-data').load('view/setting/setting.html');
    navActive('setting');
});


new Vue({
    el: '#navbarUsr',
    data:{
        usuario: [],
        wUser: false
    },
    methods:{
        userdata(){
            let state=this.wUser
            if(state){
                console.log('abrir panel de usuario')
            }else{
                console.log('cerrar panel de usuario')
            }
        }
    },
    created() {
        axios.get('backend/session.php')
        .then(resp=>{
            console.log(resp.data)
            this.usuario=resp.data
        })
        .catch(e=>{
            console.log(e)
        })
    },
})