new Vue({
    el: '#app',
    data:{
        title: 'Mis Productos',
        listaProductos: [],
        listaProductosTemp: [],
        listaPackProductos: [],
        listaInsumoProductos: [],
        categorias:[],
        delProd: null,
        editProd: false,
        productEdit: {},
        categoriasProducto: [
            {
                "idcategorias": 1,
                "nombre": 'AROS',
            },
            {
                "idcategorias": 2,
                "nombre": 'COLLARES',
            },
            {
                "idcategorias": 3,
                "nombre": 'PULSERAS',
            },
        ],
        costoTotalPack:0,
        costoTotalInsumos:0,
        costoHoras:0,
        busqInsumo: '',
        listInsumos: [],
        insumo:'',
        listaPack:[],
        setting: null,
        porcentajeGanancia: 0,
        precioHora: 0,
        cargarProducto: false,
        productDuplicado: null,
        modifyInsumo:{
            "position":null,
            "insumo":{}
        },
        buscarProductos: '',
        busquedaProductos:false,
        checkListaCombinaciones: true,
        listaCombinaciones: [],
        insumosComb: [],
        listColor: [],
        miCombinacion:{
            "primario":{
                "color":null,
                "insumos_idinsumos": []
            },
            "secundario":{
                "color":null,
                "insumos_idinsumos": []
            }
        },
        listInsumosTipComb:[],
        indexDeleteComb:null,
        indexEditComb:null,
        saving:false,
        lastCheckEdit: null,
        response: false,
        filters:{
            categoria: {
                state: false,
                value: null
            },
            year: {
                state: false,
                value: null
            },
        },
        yearProduct: [],
    },
    methods: {
        async fileSelected(event){
            let preview=document.getElementById('imgPreview')
            let file = event.target.files[0]

            // let toDataURL = url => fetch(url)
            //     .then(response => response.blob())
            //     .then(blob => new Promise((resolve, reject) => {
            //         let reader = new FileReader()
            //         reader.onloadend = () => resolve(reader.result)
            //         reader.onerror = reject
            //         reader.readAsDataURL(blob)
            //     }))
            // toDataURL(URL.createObjectURL(file))
            // .then(dataUrl => {
            //     this.productEdit.imagen = dataUrl
            // })

            let fd = new FormData()
            fd.append('image',file,file.name)
            await axios.post('backend/resize.php',fd)
            .then(resp=>{
                console.log(resp.data)
                let dataUrl = resp.data.blob
                this.productEdit.imagen = dataUrl
            })
            .catch(e=>{
                console.log(e)
            })



            let reader  = new FileReader();

            reader.onloadend = function () {
                preview.src = reader.result;
            }

            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        },
        async getProductos(param){
            let valor=null
            this.cargarProducto=true
            if(param!=undefined){
                valor=param
            }
            let data={'parametro': valor}
            await axios.post('backend/getProductos.php', data)
            .then(resp=>{
                this.listaProductos=resp.data
                this.listaProductosTemp=resp.data
                console.log(this.listaProductos)
                this.cargarProducto=false
            })
            .catch(e=>{
                console.log(e)
            })
        },
        getPackProducto(param){
            this.costoTotalPack=0
            let valor=null
            if(param!=undefined){
                valor=param
            }
            let data={'parametro': valor}
            axios.post('backend/getPackProducto.php', data)
            .then(resp=>{
                this.listaPackProductos=resp.data
                console.log(this.listaPackProductos)
                this.costoTotalPack=0
                for (let index = 0; index < this.listaPackProductos.length; index++) {
                    this.costoTotalPack+=parseFloat(this.listaPackProductos[index].precioUnitario)
                }
            })
            .catch(e=>{
                console.log(e)
            })
        },
        getInsumoProductos(param){
            let valor=null
            if(param!=undefined){
                valor=param
            }
            let data={'parametro': valor}
            axios.post('backend/getInsumoProducto.php', data)
            .then(resp=>{
                this.listaInsumoProductos=resp.data
                console.log(this.listaInsumoProductos)
                this.costoTotalInsumos=0
                for (let index = 0; index < this.listaInsumoProductos.length; index++) {
                    this.costoTotalInsumos+=parseFloat(this.listaInsumoProductos[index].Costo)
                }
            })
            .catch(e=>{
                console.log(e)
            })
        },
        getSetting(){
            axios.get('backend/getSetting.php')
            .then(resp=>{
                this.setting = resp.data
                console.log(this.setting)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        settingCategoria(id){
            let cat = id
            switch (cat) {
                case 1:
                    this.porcentajeGanancia=parseFloat(this.setting[0].porcentajeGanancia)
                    this.precioHora=parseFloat(this.setting[0].precioHoras)
                    break;
                case 2:
                        this.porcentajeGanancia=parseFloat(this.setting[1].porcentajeGanancia)
                        this.precioHora=parseFloat(this.setting[1].precioHoras)
                        break;
                case 3:
                        this.porcentajeGanancia=parseFloat(this.setting[2].porcentajeGanancia)
                        this.precioHora=parseFloat(this.setting[2].precioHoras)
                        break;
            }
            console.log(this.porcentajeGanancia)
            console.log(this.precioHora)
        },
        deleteProducto(idprodcuto){
            this.delProd=idprodcuto;
        },
        confirmarDeleteProducto(){
            let data={"idproducto":this.delProd}
            axios.post('backend/deleteProducto.php',data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-producto')

                if(resp.data.estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>${resp.data.message}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>${resp.data.message}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.delProd=null
            this.listaProductos=[]
            this.getProductos()
        },
        async duplicarProducto(product){
            await this.getInsumoProductos(product.idproductos)
            await this.getPackProducto(product.idproductos)
            this.productDuplicado=product
        },
        async confirmarDuplicarProducto(){
            console.log('Confirma Duplicado')
            this.productDuplicado.insumos=this.listaInsumoProductos
            this.productDuplicado.packaging=this.listaPackProductos
            console.log(this.productDuplicado)
            let data=this.productDuplicado
            await axios.post('backend/duplicateProduct.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-producto')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })

            // se resetean variables
            this.listaProductos=[]
            this.listaInsumoProductos=[]
            this.listaPackProductos=[]
            this.productDuplicado=null
            this.getProductos()
            this.backToTop()
        },
        editarProducto(product,index){
            this.checkEltoEdit(index)
            this.editProd=true
            this.productEdit=product
            console.log(this.productEdit)
            this.settingCategoria(parseInt(product.idcategorias))
            this.costoHoras=product.horasTrabajo*this.precioHora
            console.log('costo horas',this.costoHoras)
            console.log('porcentaje de Ganancias',this.porcentajeGanancia)
            this.productEdit.PrecioSugerido=Math.round(product.PrecioSugerido*100)/100
            console.log('precio sugerido', this.productEdit.PrecioSugerido)
            this.getInsumoProductos(product.idproductos)
            this.getPackProducto(product.idproductos)
            this.getPack()
        },
        checkEltoEdit(index){
            let idElto = 'elto'+index
            let elto = document.getElementById(idElto)
            elto.classList.add('table-warning')
            if(this.lastCheckEdit===null){
                this.lastCheckEdit=index
            }else{
                this.unCheckEltoEdit(index)
            }
        },
        unCheckEltoEdit(newCheckEdit){
            if(this.lastCheckEdit!=newCheckEdit){
                let idElto = 'elto'+this.lastCheckEdit
                let elto = document.getElementById(idElto)
                elto.classList.remove('table-warning')
                this.lastCheckEdit=newCheckEdit
            }
        },
        getPack(param){
            let val=param
            if(val===undefined){
                val=null
            }
            let data={"parametro":val}
            axios.post('backend/getPackaging.php', data)
            .then(resp=>{
                console.log(resp.data)
                this.listaPack=resp.data
            })
            .catch(e=>{
                console.log(e)
            })
        },
        getListaPack(){
            let listPackProd = this.listaPackProductos
            let listPack= this.listaPack
            console.log('lista de pack del prod',listPackProd)
            console.log('lista de packaging',listPack)
            for (let i = 0; i < listPackProd.length; i++) {
                let idpack=listPackProd[i].idpackaging
                listPack=listPack.filter(listPack => listPack.idpackaging!=idpack)
            }
            this.listaPack = listPack
            console.log('pack disponibles',listPack)
        },
        async addProducto(){
            this.response=true
            let data = this.productEdit
            data.insumos = this.listaInsumoProductos
            data.packaging = this.listaPackProductos
            data.combinaciones = this.listaCombinaciones
            console.log('editar:',data)
            await axios.post('backend/editProducto.php',data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alert-producto')

                if(resp.data.Estado===true){
                    alerta.innerHTML=`
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close" @click="response=false">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }else{
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.Data}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.editProd=false
            //this.listaProductos=[]
            //this.getProductos()
            //this.backToTop()
        },
        setBackgraund(cat){
            let fondos=this.categorias
            let myCat=cat
            for (let i = 0; i < fondos.length; i++) {
                const element = fondos[i];
                if(element.categoria===myCat){
                    return element.bg
                }
            }
        },
        getCategorias(){
            axios.post('backend/getCategories.php')
            .then(resp=>{
                this.categorias=resp.data
                console.log(this.categorias)
            })
        },
        precioMayorista(){
            this.productEdit.precioMayorista=Math.round((this.productEdit.precioMinorista*0.6)*100)/100
        },
        backToTop(){
            $('html,body').stop().animate({
              scrollTop: 0
            }, 'slow', 'swing');
        },
        modificarHoras(){
            console.log(this.productEdit.horasTrabajo)
            this.costoHoras=this.productEdit.horasTrabajo*this.precioHora
            this.precioSugerido()
        },
        getInsumos(param){
            if(param!=''){
                let data = {"parametro": param}
                axios.post('backend/getInsumosProducto.php', data)
                .then(resp=>{
                    console.log(resp.data)
                    this.listInsumos=resp.data
                })
                .catch(e=>{
                    console.log(e)
                })
            }else{
                this.listInsumos= []
            }
        },
        selectInsumo(insumo){
            let newInsumo = insumo
            newInsumo.cantidadInsumo = 0
            console.log(insumo)
            this.listInsumos=[insumo]
            this.cargarInsumo=true
            this.insumo=newInsumo
            this.busqInsumo=''
        },
        quitarSeleccion(){
            this.listInsumos=[]
            this.cargarInsumo=false
            this.insumo=''
        },
        addInsumo(){
            this.insumo.Costo=Math.round((this.insumo.cantidadInsumo*parseFloat(this.insumo.costoUnitario))*100)/100
            this.costoTotalInsumos+=this.insumo.Costo
            this.insumo.insumos_idinsumos=this.insumo.idinsumos
            this.listaInsumoProductos.push(this.insumo)
            this.precioSugerido()
            this.insumo=''
            this.listInsumos=[]
            this.cargarInsumo=false
            console.log('Insumo agregado a producto')
        },
        quitarInsumo(insumo,i){    
            this.costoTotalInsumos-=insumo.Costo
            this.listaInsumoProductos.splice(i,1)
            this.precioSugerido()
        },
        quitarPack(packProducto,i){
            this.listaPackProductos.splice(i,1)
            this.costoTotalPack-=packProducto.precioUnitario
            this.precioSugerido()
            this.listaPack.push(packProducto)
        },
        addPackProd(pack,i){
            this.listaPackProductos.push(pack)
            this.listaPack.splice(i,1)
            this.costoTotalPack+=parseFloat(pack.precioUnitario)
            this.precioSugerido()
        },
        precioSugerido(){
            this.productEdit.PrecioSugerido=Math.round(((this.costoHoras+this.costoTotalInsumos+this.costoTotalPack)*this.porcentajeGanancia)*100)/100
        },
        cancelarEdicion(){
            this.editProd=false
            //this.listaProductos=[]
            //this.getProductos()
            //this.backToTop()
        },
        modificarInsumo(insumo,index){
            this.modifyInsumo.position=index
            this.modifyInsumo.insumo=insumo
            console.log('Modificar Insumo:', this.modifyInsumo)
        },
        confirmarModify(){
            console.log('costo viejo', this.modifyInsumo.insumo.Costo)
            console.log('costo total viejo', this.costoTotalInsumos)
            console.log('precio sugerido viejo', this.productEdit.PrecioSugerido)
            this.costoTotalInsumos-=this.modifyInsumo.insumo.Costo
            this.modifyInsumo.insumo.Costo=Math.round((this.modifyInsumo.insumo.cantidadInsumo*parseFloat(this.modifyInsumo.insumo.costoUnitario))*100)/100
            this.costoTotalInsumos+=this.modifyInsumo.insumo.Costo
            this.precioSugerido()
            console.log('costo nuevo', this.modifyInsumo.insumo.Costo)
            console.log('costo total nuevo', this.costoTotalInsumos)
            console.log('precio sugerido nuevo', this.productEdit.PrecioSugerido)
            console.log('Insumo Modificado:', this.modifyInsumo)
            this.listaInsumoProductos[this.modifyInsumo.position]=this.modifyInsumo.insumo
            this.modifyInsumo = {
                "position":null,
                "insumo":{}
            }
            console.log('Reset modify:', this.modifyInsumo)
        },
        buscarProd(){
            this.busquedaProductos=true
            this.getProductos(this.buscarProductos)
        },
        limpiarBusqueda(){
            this.buscarProductos=''
            this.busquedaProductos=false
            this.getProductos()
        },
        habilitar(idfiltro){
            let id = idfiltro
            if(id==='filtrocategoria'){
                if(this.filters.categoria.state){
                    document.getElementById('filtrocategoria').disabled = false
                }else{
                    document.getElementById('filtrocategoria').disabled = true
                }
            }
            if(id==='filtroyear'){
                if(this.filters.year.state){
                    document.getElementById('filtroyear').disabled = false
                }else{
                    document.getElementById('filtroyear').disabled = true
                }
            }
        },
        filterList(){
            this.listaProductos=this.listaProductosTemp

            if(this.filters.categoria.state&&!((this.filters.categoria.value===null)||(this.filters.categoria.value==='null'))){
                this.listaProductos = this.listaProductos.filter(item => item.categoria === this.filters.categoria.value)
            }

            if(this.filters.year.state&&!((this.filters.year.value===null)||(this.filters.year.value==='null'))){
                this.listaProductos = this.listaProductos.filter(item => item.year === this.filters.year.value)
            }
        },
        quitarFiltros(){
            this.filters.categoria.state=false
            this.filters.categoria.value=null
            this.filters.year.state=false
            this.filters.year.value=null
            document.getElementById('filtrocategoria').disabled = true
            document.getElementById('filtroyear').disabled = true
            this.listaProductos=this.listaProductosTemp
        },
        async getYearProductos(){
            await axios.get('backend/getYearProduct.php')
                .then(resp=>{
                    console.log(resp.data)
                    this.yearProduct=resp.data
                })
                .catch(e=>{
                    console.log(e)
                })
        },
        // metodos para la carga de combinaciones
            checkCombinaciones(){
                let comb=this.listaCombinaciones
                if(comb.length>0){
                    this.checkListaCombinaciones=true
                }else{
                    this.checkListaCombinaciones=false
                }
            },
            cargarCombInsumos(){
                this.insumosComb = []
                this.listInsumosTipComb = []
                let insumos = this.listaInsumoProductos.filter(item => (item.categoria === 'CRISTALES') || (item.categoria === 'PIEDRAS'))
                this.insumosComb = insumos
                for (let i = 0; i < this.insumosComb.length; i++) {
                    let object = {
                        "insumos_idinsumos":this.insumosComb[i].insumos_idinsumos,
                        "tipo":0
                    }
                    this.listInsumosTipComb.push(object)
                }
                console.log(this.insumosComb)
                console.log(this.listInsumosTipComb)
            },
            getColor(){
                axios.post('backend/getColor.php')
                .then(resp=>{
                    let data=resp.data
                    for (let i = 0; i < data.length; i++) {
                        this.listColor[i] = {
                            "color":data[i].color
                        }
                    }
                    console.log(this.listColor)
                })
                .catch(e=>{
                    console.log(e)
                })
            },
            removeItemFromArr( arr, item ){
                let i = arr.indexOf( item )
    
                if ( i !== -1 ) {
                    arr.splice( i, 1 )
                }
            },
            cargarInsTipoComb(id){
                let insumo = this.listInsumosTipComb.filter(item => item.insumos_idinsumos === id)
                console.log(insumo)
                this.removeItemFromArr(this.miCombinacion.primario.insumos_idinsumos,id)
                this.removeItemFromArr(this.miCombinacion.secundario.insumos_idinsumos,id)
                switch (insumo[0].tipo) {
                    case 1:
                        this.miCombinacion.primario.insumos_idinsumos.push(id)
                        break;
                    case 2:
                        this.miCombinacion.secundario.insumos_idinsumos.push(id)
                        break;
                }
                console.log(this.miCombinacion.primario.insumos_idinsumos)
                console.log(this.miCombinacion.secundario.insumos_idinsumos)
            },
            addCombinacion(){
                this.listaCombinaciones.push(this.miCombinacion)
                this.miCombinacion = {
                    "primario":{
                        "color":null,
                        "insumos_idinsumos": []
                    },
                    "secundario":{
                        "color":null,
                        "insumos_idinsumos": []
                    }
                }
                for (let i = 0; i < this.listInsumosTipComb.length; i++) {
                    this.listInsumosTipComb[i].tipo=0
                }
            },
            editarComb(comb, index){
                console.log('editar',comb)
                this.miCombinacion=comb
                this.indexEditComb=index
                let primario = comb.primario.insumos_idinsumos
                let secundario = comb.secundario.insumos_idinsumos
                for (let i = 0; i < primario.length; i++) {
                    let tipo = this.listInsumosTipComb.filter(item => item.insumos_idinsumos === primario[i])
                    tipo[0].tipo=1
                }
                for (let i = 0; i < secundario.length; i++) {
                    let tipo = this.listInsumosTipComb.filter(item => item.insumos_idinsumos === secundario[i])
                    tipo[0].tipo=2
                }
                console.log(this.listInsumosTipComb)
            },
            addModifCombinacion(){
                let index = this.indexEditComb
                this.listaCombinaciones.splice(index,1,this.miCombinacion)
                
                //Resetear variables
                this.miCombinacion = {
                    "primario":{
                        "color":null,
                        "insumos_idinsumos": []
                    },
                    "secundario":{
                        "color":null,
                        "insumos_idinsumos": []
                    }
                }
                for (let i = 0; i < this.listInsumosTipComb.length; i++) {
                    this.listInsumosTipComb[i].tipo=0
                }
                this.indexEditComb=null
            },
            cancelModifCombinacion(){
                this.indexEditComb=null
                this.cancelarCombinacion()
            },
            cancelarCombinacion(){
                this.miCombinacion = {
                    "primario":{
                        "color":null,
                        "insumos_idinsumos": []
                    },
                    "secundario":{
                        "color":null,
                        "insumos_idinsumos": []
                    }
                }
                for (let i = 0; i < this.listInsumosTipComb.length; i++) {
                    this.listInsumosTipComb[i].tipo=0
                }
            },
            borrarComb(comb,index){
                console.log('borrar',comb, 'index:', index)
                this.miCombinacion=comb
                this.indexDeleteComb=index
            },
            deleteComb(){
                if(this.indexDeleteComb!==null){
                    this.listaCombinaciones.splice( this.indexDeleteComb, 1 )
                }
            },
            cancelDeleteComb(){
                this.indexDeleteComb=null
            },
            async getCombinaciones(id){
                this.listaCombinaciones=[]
                let data = {
                    parametro: parseInt(id)
                }
                console.log(data)
                await axios.post('backend/getCombinacion.php', data)
                .then(resp=>{
                    let combinacion = resp.data
                    console.log(combinacion)
                    // Si hay combinaciones en BD cargo el array de combinaciones
                    if(combinacion.length>0){
                        console.log('hay combinaciones')
                        let idcomb=combinacion[0].idcombinaciones_prod
                        console.log(idcomb)
                        for (let i = 0; i < combinacion.length; i++) {
                            if(combinacion[i].idcombinaciones_prod===idcomb){
                                console.log('match')
                                if(combinacion[i].tipo_comb==="1"){
                                    this.miCombinacion.primario.color=combinacion[i].color
                                    this.miCombinacion.primario.insumos_idinsumos.push(combinacion[i].insumos_idinsumos)
                                }else{
                                    this.miCombinacion.secundario.color=combinacion[i].color
                                    this.miCombinacion.secundario.insumos_idinsumos.push(combinacion[i].insumos_idinsumos)
                                }
                            }else{
                                idcomb=combinacion[i].idcombinaciones_prod
                                this.listaCombinaciones.push(this.miCombinacion)
                                this.miCombinacion = {
                                    "primario":{
                                        "color":null,
                                        "insumos_idinsumos": []
                                    },
                                    "secundario":{
                                        "color":null,
                                        "insumos_idinsumos": []
                                    }
                                }
                                if(combinacion[i].tipo_comb==="1"){
                                    this.miCombinacion.primario.color=combinacion[i].color
                                    this.miCombinacion.primario.insumos_idinsumos.push(combinacion[i].insumos_idinsumos)
                                }else{
                                    this.miCombinacion.secundario.color=combinacion[i].color
                                    this.miCombinacion.secundario.insumos_idinsumos.push(combinacion[i].insumos_idinsumos)
                                }
                            }
                        }
                        this.listaCombinaciones.push(this.miCombinacion)
                        console.log('lista de combinaciones:',this.listaCombinaciones)
                        this.miCombinacion = {
                            "primario":{
                                "color":null,
                                "insumos_idinsumos": []
                            },
                            "secundario":{
                                "color":null,
                                "insumos_idinsumos": []
                            }
                        }
                    }else{
                        console.log('no hay combinaciones')
                        this.miCombinacion = {
                            "primario":{
                                "color":null,
                                "insumos_idinsumos": []
                            },
                            "secundario":{
                                "color":null,
                                "insumos_idinsumos": []
                            }
                        }
                    }
                })
                .catch(e=>{
                    console.log(e)
                })
            }
        // fin metodos para la carga de combinaciones
    },
    created() {
        this.getSetting()
        this.getProductos()
        this.getColor()
        this.getYearProductos()
    },
    mounted() {
        this.getCategorias()
        let keyupBusqInsumo=document.getElementById('search-insumo')
        keyupBusqInsumo.addEventListener('keyup', ()=>{
            this.getInsumos(this.busqInsumo)
        })
    },
    updated() {
        //precio mayorista
        let keyupPrecioMin=document.getElementById('editPrecioMinoristaProducto')
        keyupPrecioMin.addEventListener('keyup',()=>{
            this.precioMayorista()
        })
        keyupPrecioMin.addEventListener('click',()=>{
            this.precioMayorista()
        })

        let keyupHoras = document.getElementById('horasTrabajoProducto')
        keyupHoras.addEventListener('keyup',()=>{
            this.modificarHoras()
        })

        this.checkCombinaciones()

        this.getYearProductos()
    },
})