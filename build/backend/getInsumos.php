<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $parametro=$data['parametro'];

    // Listar insumos
    if(empty($parametro) || $parametro==='' || $parametro===null){
        $sql="SELECT i.*,c.*,p.nombre as nombreProv FROM insumos AS i INNER JOIN colores AS c INNER JOIN proveedores AS p ON i.idinsumos=c.insumos_idinsumos AND i.proveedores_idproveedor=p.idproveedor ORDER BY i.categoria,p.nombre,i.nombre";
        $sql_get=$pdo->prepare($sql);
        $sql_get->execute();
    }else{
        $sql="SELECT i.*,c.*,p.nombre as nombreProv FROM insumos AS i INNER JOIN colores AS c INNER JOIN proveedores AS p ON i.idinsumos=c.insumos_idinsumos AND i.proveedores_idproveedor=p.idproveedor AND ((i.categoria LIKE '%$parametro%') OR (i.nombre LIKE '%$parametro%') OR (i.detalle LIKE '%$parametro%') OR (c.color LIKE '%$parametro%')) ORDER BY i.categoria,i.nombre,p.nombre";
        $sql_get=$pdo->prepare($sql);
        $sql_get->execute();
    }
    
    $response=$sql_get->fetchAll();
    echo json_encode($response);
?>