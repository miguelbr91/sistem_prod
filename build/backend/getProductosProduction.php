<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $idCategoriaProduccion=$data['idCategoriaProduccion'];
    
    // Listar Colores insumo
    $sql="SELECT * FROM vw_productosProduction WHERE idCategoriaProduccion=$idCategoriaProduccion ORDER BY nombre,orden";
    $sql_get=$pdo->prepare($sql);
    $sql_get->execute();
    $productos=$sql_get->fetchAll();

    $item=[
        "productos_idproductos"=>$productos[0]['productos_idproductos'],
        "imagen"=>$productos[0]['imagen'],
        "nombre"=>$productos[0]['nombre'],
        "nombreCategoria"=>$productos[0]['nombreCategoria'],
        "idcategorias"=>$productos[0]['idcategorias'],
        "colores"=>[]
    ];

    $response=[];

    for ($i=0; $i < sizeof($productos) ; $i++) { 
        if($item['productos_idproductos']!=$productos[$i]['productos_idproductos']){
            array_push($response,$item);
            $item=[
                "productos_idproductos"=>$productos[$i]['productos_idproductos'],
                "imagen"=>$productos[$i]['imagen'],
                "nombre"=>$productos[$i]['nombre'],
                "nombreCategoria"=>$productos[$i]['nombreCategoria'],
                "idcategorias"=>$productos[$i]['idcategorias'],
                "colores"=>[]
            ];
        }
        $color=[
            "color"=>$productos[$i]['color'],
            "cantidad"=>$productos[$i]['cantidad'],
            "realizado"=>$productos[$i]['realizado'],
            "orden"=>$productos[$i]['orden']
        ];
        // if($productos[$i]['color']!='MULTICOLOR'){
        //     $color=[
        //         "color"=>$productos[$i]['color'],
        //         "cantidad"=>$productos[$i]['cantidad']
        //     ];
        // }else{
        //     if($productos[$i]['cantidad']>0){
        //         $idprodMulti=$productos[$i]['productos_idproductos'];
        //         $sql="SELECT * FROM vw_settingProductoMulticolor WHERE productos_idproductos=? AND idCategoriaProduccion=?";
        //         $sql_get_conf=$pdo->prepare($sql);
        //         $sql_get_conf->execute(array($idprodMulti,$idCategoriaProduccion));
        //         $conf=$sql_get_conf->fetchAll();

        //         $conf_total=[];
        //         $idConfProdMulti=$conf[0]['idconfiguracionProductoMulticolor'];
        //         $cantConfig=$conf[0]['cantidadConfigurada'];
        //         $conf_n=[
        //             "cantidadConfigurada"=>$cantConfig,
        //             "combinaciones"=>[]
        //         ];
        //         $combIns=[
        //             "idinsumos"=>$conf[0]['insumos_idinsumos'],
        //             "nombre"=>$conf[0]['nombre'],
        //             "detalle"=>$conf[0]['detalle'],
        //             "cantidad"=>$conf[0]['cantidadCombinacion'],
        //             "color"=>$conf[0]['color']
        //         ];
        //         array_push($conf_n['combinaciones'],$combIns);
        //         // Recorrido del toda la configuracion del producto
        //         for ($j=1; $j < sizeof($conf); $j++) { 

        //             // Verifico el cambio de configuracion de producto
        //             if($idConfProdMulti!=$conf[$j]['idconfiguracionProductoMulticolor']){
        //                 array_push($conf_total,$conf_n);
        //                 $idConfProdMulti=$conf[$j]['idconfiguracionProductoMulticolor'];
        //                 $cantConfig=$conf[$j]['cantidadConfigurada'];
        //                 $conf_n=[
        //                     "cantidadConfigurada"=>$cantConfig,
        //                     "combinaciones"=>[]
        //                 ];
        //             }

        //             // Configuro lista de combinacion
        //             $combIns=[
        //                 "idinsumos"=>$conf[$j]['insumos_idinsumos'],
        //                 "nombre"=>$conf[$j]['nombre'],
        //                 "detalle"=>$conf[$j]['detalle'],
        //                 "cantidad"=>$conf[$j]['cantidadCombinacion'],
        //                 "color"=>$conf[$j]['color']
        //             ];
        //             array_push($conf_n['combinaciones'],$combIns);
                    
        //         }

        //         // Al finalizar cargo los dos sub-arreglos que no se accedio en los condicionales en la ultima iteracion

        //         array_push($conf_total,$conf_n);

        //         $conf_n=[];
        //         $combIns=[];

        //         $color=[
        //             "color"=>$productos[$i]['color'],
        //             "cantidad"=>$productos[$i]['cantidad'],
        //             "configuracion"=>$conf_total
        //         ];
        //     }
        // }
        array_push($item['colores'],$color);
    }
    array_push($response,$item);

    echo json_encode($response);
?>