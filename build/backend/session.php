<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    session_start();
    if(!empty($_SESSION['usuario'])){
        $usr = [
            "name" => $_SESSION['usuario'],
            "mail" => $_SESSION['mail'],
            "type" => $_SESSION['type']
        ];
        echo json_encode($usr);
    }else{
        $usr = ["name" => null];
        echo json_encode($usr);
    }
?>