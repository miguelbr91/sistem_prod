<?php
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $user_name=$data['name'];
    $user_pass=$data['pass'];
    include_once "conn.php";
    $sql="SELECT * FROM user WHERE `name`=?";
    $sesion_sql=$pdo->prepare($sql);
    $sesion_sql->execute(array($user_name));
    $sesion_user=$sesion_sql->fetch();
    if(!empty($sesion_user)){
        if(password_verify($user_pass,$sesion_user['pass'])){
            $usuario=[
                "nombre" => $sesion_user['name'],
                "mail" => $sesion_user['mail'],
                "type" => $sesion_user['type']
            ];
            session_start();
            $_SESSION['usuario']=$usuario['nombre'];
            $_SESSION['mail']=$usuario['mail'];
            $_SESSION['type']=$usuario['type'];
            echo json_encode($usuario);
        }else{
            echo json_encode(["error"=>"Contraseña incorrecta."]);
        }
    }else{
        echo json_encode(["error"=>"El usuario no existe."]);
    }
?>