<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $idCategoriaProduccion=$data['parametro'];
    
    $response=[];

    $sql="SELECT * FROM vw_insumosProduccion WHERE idCategoriaProduccion=?";
    
    
    $sql_get=$pdo->prepare($sql);
    $sql_get->execute(array($idCategoriaProduccion));
    $insumosProduccion=$sql_get->fetchAll();

    for ($i=0; $i < sizeof($insumosProduccion); $i++) { 
        $item=$insumosProduccion[$i];
        $idInsumo=$item['insumos_idinsumos'];
        $colorInsumo=$item['color'];
        $sql="SELECT * FROM colores WHERE insumos_idinsumos=? and color=?";
        $sql_get=$pdo->prepare($sql);
        $sql_get->execute(array($idInsumo,$colorInsumo));
        $stockColor=$sql_get->fetchAll();
        if(sizeof($stockColor)>0){
            
            if($item['TotalInsumo']>$stockColor[0]['stock']){
                $faltante = $item['TotalInsumo'] - $stockColor[0]['stock'];
            }else{
                $faltante = 0;
            }

            $elemento=[
                "insumos_idinsumos" => $item['insumos_idinsumos'],
                "codigoInsumo" => $item['codigoInsumo'],
                "nombreInsumo" => $item['nombreInsumo'],
                "color" => $item['color'],
                "detalle" => $item['detalle'],
                "cantInsumoXunidad" => $item['cantInsumoXunidad'],
                "TotalInsumo" => $item['TotalInsumo'],
                "cantDisponible" => $stockColor[0]['stock'],
                "cantFaltante" => $faltante
            ];
        }else{
            $elemento=[
                "insumos_idinsumos" => $item['insumos_idinsumos'],
                "codigoInsumo" => $item['codigoInsumo'],
                "nombreInsumo" => $item['nombreInsumo'],
                "color" => $item['color'],
                "detalle" => $item['detalle'],
                "cantInsumoXunidad" => $item['cantInsumoXunidad'],
                "TotalInsumo" => $item['TotalInsumo'],
                "cantDisponible" => 0,
                "cantFaltante" => $item['TotalInsumo']
            ];
        }
        array_push($response,$elemento);
    }

    echo json_encode($response);
?>