<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    $id=$data["id"];
    
    $sql="DELETE FROM proveedores WHERE idproveedor = (?)";
    $delete_sql=$pdo->prepare($sql);
    $delete_sql->execute(array($id));
    
    if($delete_sql){
        $response = ["Data" => 'Se ha eliminado correctamente.' , "Estado" => true];
    }else{
        $response = ["Data" => 'Error! no se pudo eliminar' , "Estado" => false];
    }

    $response = json_encode($response);
    echo $response; 
?>