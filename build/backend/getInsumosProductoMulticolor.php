<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $parametro=$data['parametro'];
    $response=[];

    // Listar Colores insumo
    if(empty($parametro) || $parametro==='' || $parametro===null){
        $response=null;
    }else{
        $sql="SELECT ixp.productos_idproductos as idproductos,ixp.insumos_idinsumos as idinsumos,ixp.nombre,ixp.categoria,ixp.detalle,ixp.cantidadInsumo FROM insumosXproductos as ixp WHERE ixp.productos_idproductos=$parametro and ixp.categoria in ('CRISTALES','PIEDRAS')";
        $sql_get=$pdo->prepare($sql);
        $sql_get->execute();
        while($insumos=$sql_get->fetch(PDO::FETCH_ASSOC)){
            $item=[
                "idproductos"=>$insumos['idproductos'],
                "idinsumos"=>$insumos['idinsumos'],
                "nombre"=>$insumos['nombre'],
                "categoria"=>$insumos['categoria'],
                "detalle"=>$insumos['detalle'],
                "cantidadInsumo"=>intval($insumos['cantidadInsumo']),
                "cantInsumoXconf"=>intval($insumos['cantidadInsumo']),
                "combinacion"=>[
                    [
                        "color"=>null,
                        "cantidad"=>0,
                        "saveConfiguracionColor"=>false
                    ]
                ]
            ];
            array_push($response,$item);
        }
    }
    


    echo json_encode($response);
?>