<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    include_once 'validarData.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    $id=$data['idpackaging'];
    $nombre=$data['nombre'];
    $stock=$data['stock'];
    $precioUnitario=$data['precioUnitario'];
    $img=$data['img'];

    if($id != null){
        $sql="UPDATE packaging SET nombre=?,stock=?,precioUnitario=?,img=? WHERE idpackaging=?";
        $sql_update=$pdo->prepare($sql);
        $sql_update->execute(array($nombre,$stock,$precioUnitario,$img,$id));
    
        if($sql_update){
            $response=[
                "estado"=>true,
                "message"=>'El packaging ha sido actualizado correctamente'
            ];
        }else{
            $response=[
                "estado"=>false,
                "message"=>'Error! El packaging no se ha podido actualizar'
            ];
        }
    }else{
        $sql="INSERT INTO packaging(nombre,stock,precioUnitario,img) VALUES (?,?,?,?)";
        $sql_insert=$pdo->prepare($sql);
        $sql_insert->execute(array($nombre,$stock,$precioUnitario,$img));
    
        if($sql_insert){
            $response=[
                "estado"=>true,
                "message"=>'El packaging ha sido cargado correctamente'
            ];
        }else{
            $response=[
                "estado"=>false,
                "message"=>'Error! El packaging no se ha podido registar'
            ];
        }
    }
    echo json_encode($response);
?>