<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    
    $json = file_get_contents("php://input");
    $data = json_decode($json, true);
    
    $idproduction = intval($data['idproduction']);
    $idCategoriaProduccion = intval($data['idCategoriaProduccion']);
    $productos = $data['productos'];
    $updated = true;
    $totalProducido = 0;

    for ($i=0; $i < sizeof($productos); $i++) { 
        $item = $productos[$i];        
        $idproductos = intval($item['productos_idproductos']);
        $colores = $item['colores'];
        
        for ($j=0; $j < sizeof($colores); $j++) { 
            $item2 = $colores[$j];
            $color = $item2['color'];
            $realizado = intval($item2['realizado']);

            $sql = "UPDATE productos_por_produccion SET realizado = ? WHERE productos_idproductos=? and idCategoriaProduccion=? and color=?";
            $update_prod=$pdo->prepare($sql);
            $update_prod->execute(array($realizado,$idproductos,$idCategoriaProduccion,$color));

            if($update_prod){
                $totalProducido += $realizado;
            }else{
                $updated = false;
            }
        }

    }
    
    if($updated){
        $sql = "UPDATE categoria_por_production SET cantidadProducida = ? WHERE production_idproduction=? and idCategoriaProduccion=?";
        $update_prod=$pdo->prepare($sql);
        $update_prod->execute(array($totalProducido,$idproduction,$idCategoriaProduccion));
        if($update_prod){
            $response = ["Data" => '<strong>Correcto!</strong> se ha actualizado correctamente.' , "Error" => false];
        }else{
            $response = ["Data" => '<strong>Error!</strong> No se ha podido actualizar.' , "Error" => true];
        }
    }else{
        $updated = false;
        $response = ["Data" => '<strong>Error!</strong> No se ha podido actualizar.' , "Error" => true];
    }

    $response = json_encode($response);
    echo $response; 
?>