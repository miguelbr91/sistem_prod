<?php
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    
    require_once 'conn.php';
    require_once 'validarData.php';

    $nombre=validar($data['nombre']);
    $nombreContacto=validar($data['nombreContacto']);
    $mail=validar($data['mail']);
    $web=validar($data['web']);
    $direccion=validar($data['direccion']);
    $ciudad=validar($data['ciudad']);
    $provincia=validar($data['provincia']);
    $comentario=validar($data['comentario']);
    $telefono=validar($data['telefono']);
    $celular=validar($data['celular']);

    
    $sql="INSERT INTO proveedores(nombre,nombreContacto,mail,web,direccion,ciudad,provincia,comentario,telefono,celular) VALUES (?,?,?,?,?,?,?,?,?,?)";
    $add_sql=$pdo->prepare($sql);
    $add_sql->execute(array($nombre,$nombreContacto,$mail,$web,$direccion,$ciudad,$provincia,$comentario,$telefono,$celular));
    
    if($add_sql){
        $response = ["Proveedor" => $nombre , "Estado" => true];
    }else{
        $response = ["Proveedor" => $nombre , "Estado" => false];
    }

    
    $response = json_encode($response);
    echo $response; 
?>