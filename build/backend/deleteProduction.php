<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';

    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    $idCategoriaProduccion=$data['idCategoriaProduccion'];
    
    $sql="DELETE FROM categoria_por_production WHERE idCategoriaProduccion=$idCategoriaProduccion";
    $delete_sql=$pdo->prepare($sql);
    $delete_sql->execute();
    
    if($delete_sql){
        $response = ["Data" => '<strong>Correcto!</strong> se ha eliminado correctamente la producción.' , "Estado" => true];
    }else{
        $response = ["Data" => '<strong>Error!</strong> No se ha podido borrar la producción.' , "Estado" => false];
    }

    echo json_encode($response);
?>