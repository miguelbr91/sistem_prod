<?php

    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';

    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $parametro=$data['parametro'];
    

    if(empty($parametro) || $parametro==='' || $parametro===null){
        $sql="SELECT * FROM proveedores ORDER BY nombre";
        $gsent=$pdo->prepare($sql);
        $gsent->execute();
    }else{
        $sql="SELECT * FROM proveedores WHERE nombre LIKE '%$parametro%' ORDER BY nombre";
        $gsent=$pdo->prepare($sql);
        $gsent->execute();
    }
    
    $response=$gsent->fetchAll();
    
    echo json_encode($response);

?>