<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);

    require_once 'validarData.php';

    $idColor=$data['idcolores'];
    $stock=validar($data['stock']);

    $sql="UPDATE colores SET stock=?  WHERE idcolores = ?";
    $update_stock=$pdo->prepare($sql);
    $update_stock->execute(array($stock,$idColor));
    
    if($update_stock){
        $response = ["Data" => '<strong>Correcto!</strong> Se ha actualizado el stock correctamente.' , "Estado" => true];
    }else{
        $response = ["Data" => '<strong>Error!</strong> no se pudo actualizar' , "Estado" => false];
    }

    $response = json_encode($response);
    echo $response; 
?>