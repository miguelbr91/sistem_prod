<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

$nombre=$_FILES['image']['name'];
$tmp=$_FILES['image']['tmp_name'];
$type=$_FILES['image']['type'];
$folder = '../tmp';

if($type=='image/jpeg'){
    $original=imagecreatefromjpeg($tmp);
}else if($type=='image/png'){
    $original=imagecreatefrompng($tmp);
}

$w_or=imagesx($original);
$h_or=imagesy($original);
$copia=imagecreatetruecolor($w_cpy,$h_cpy);
imagecopyresampled($copia,$original,0,0,0,0,$w_cpy,$h_cpy,$w_or,$h_or);

$resize=false;
 if(($w_or>300)||($h_or>300)){
    $resize=true;
}

if($resize){
    if($w_or>$h_or){
        $w_cpy=300;
        $h_cpy=round((300*$h_or)/$w_or);
    }else{
        $h_cpy=300;
        $w_cpy=round((300*$w_or)/$h_or);
    }    
    $copia=imagecreatetruecolor($w_cpy,$h_cpy);
    imagecopyresampled($copia,$original,0,0,0,0,$w_cpy,$h_cpy,$w_or,$h_or);
}else{
    $copia=$original;
}
$dir="$folder/$nombre";
imagejpeg($copia,$dir,70);
$arc=file_get_contents($dir);
$blob=base64_encode($arc);
@unlink($dir);
$response = [
    "blob"=>"data:image/jpeg;base64,$blob",
    "resize"=>$resize,
    "alto_or"=>$h_or,
    "ancho_or"=>$w_or,
    "alto_cpy"=>$h_cpy,
    "ancho_cpy"=>$w_cpy,
    "dir"=>$dir
];

echo json_encode($response);