<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';

    $sql="SELECT DISTINCT `year` AS `year` FROM vw_productos";

    $gsent=$pdo->prepare($sql);
    $gsent->execute();

    $response=$gsent->fetchAll();

    echo json_encode($response);
?>