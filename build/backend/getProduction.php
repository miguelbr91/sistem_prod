<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    include_once 'conn.php';
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    
    $response=[];

    $sql="SELECT * FROM production ORDER BY `year`,`mesNro`";
    $sql_get=$pdo->prepare($sql);
    $sql_get->execute();
    $producciones=$sql_get->fetchAll();
    for ($i=0; $i < sizeof($producciones) ; $i++) { 
        $item=$producciones[$i];
        $sql="SELECT * FROM vw_production WHERE idproduction=? ORDER BY `nombreCategoria`";
        $sql_get=$pdo->prepare($sql);
        $sql_get->execute(array($item['idproduction']));
        $listaProducciones=$sql_get->fetchAll();
        $listaProductionCategoria = [];
        $totalProduccionMes=0;
        $totalProducidoMes=0;
        for ($j=0; $j < sizeof($listaProducciones); $j++) { 
            $item2=$listaProducciones[$j];
            $elto2 = [
                "idCategoriaProduccion" => $item2['idCategoriaProduccion'],
                "nombreCategoria" => $item2['nombreCategoria'],
                "cantidadProducir" => $item2['cantidadProducir'],
                "cantidadProducida" => $item2['cantidadProducida'],
            ];
            $totalProduccionMes+=$item2['cantidadProducir'];
            $totalProducidoMes+=$item2['cantidadProducida'];
            array_push($listaProductionCategoria,$elto2);
        }
        $elto1=[
            "idproduction" => $item['idproduction'],
            "fecha" => $item['fecha'],
            "fecha" => $item['fecha'],
            "year" => $item['year'],
            "mesNombre" => $item['mesNombre'],
            "mesNro" => $item['mesNro'],
            "totalProduccion" => $totalProduccionMes,
            "totalProducido" => $totalProducidoMes,
            "listaProductionCategoria" => $listaProductionCategoria
        ];
        array_push($response,$elto1);
    }

    echo json_encode($response);
?>